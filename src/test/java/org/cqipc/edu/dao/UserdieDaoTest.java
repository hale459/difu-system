package org.cqipc.edu.dao;

import org.cqipc.edu.entity.Userlife;
import org.cqipc.edu.service.PlagueService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class UserdieDaoTest {

    @Autowired
    private UserdieDao userdieDao;

    @Autowired
    private UserlifeDao userlifeDao;

    @Autowired
    private DeletedDao deletedDao;

    @Autowired
    private PlagueService plagueService;

    @Autowired
    private PlagueInfoDao plagueInfoDao;

    @Autowired
    private PlagueDao plagueDao;

    @Autowired
    private DistrictDao districtDao;

    @Autowired
    private MengpoDao mengpoDao;


    @Test
    void test() {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(7);
        Integer integer = deletedDao.batchDelDeleted(list);
        System.out.println("成功 = " + integer);
    }

    @Test
    void test2() {
        for (Userlife userlife : userlifeDao.getUserlifeByAreaId(8)) {
            System.out.println("userlife = " + userlife);
        }
    }

    /**
     * @Description: 测试散布瘟疫
     * @Author: Heyue
     * @Date: 2020/6/26
     */
    @Test
    void test3() {
        System.out.println(plagueService.sendPlague(6, 8, 110, "你好"));
    }

    @Test
    void test4() {
        System.out.println("mengpoDao.getMengpoById(3).getFFid() = " + mengpoDao.getMengpoById(3).getFFid());
    }
}

