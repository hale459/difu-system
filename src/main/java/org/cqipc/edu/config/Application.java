package org.cqipc.edu.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @Description: Druid连接池配置
 * @Author: Heyue
 * @Date: 2020/7/2
 */
@Configuration
public class Application {

    /**
     * 注入druid数据源并设置不销毁
     * destroyMethod：将类中的destroy方法设置为关闭，即不销毁(当数据框连接不使用时，把该连接放回数据池方便下次调用)
     * initMethod:设置bean在初始化时就执行该方法
     * ConfigurationProperties：设置配置信息前缀
     *
     * @return
     */
    @Bean(destroyMethod = "close", initMethod = "init")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource getDataSource() {
        return new DruidDataSource();
    }
}
