package org.cqipc.edu.config;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @Description: 无权限异常统一处理
 * @ClassName: NoPermissionExceptionConfig
 * @Author: Heyue
 * @DateTime: 2020-07-03
 **/
@ControllerAdvice
public class NoPermissionExceptionConfig {

    @ExceptionHandler(UnauthorizedException.class)
    public String handleShiroException(Exception ex) {
        return "redirect:/403";
    }

    @ExceptionHandler(AuthorizationException.class)
    public String AuthorizationException(Exception ex) {
        return "redirect:/403";
    }


}
