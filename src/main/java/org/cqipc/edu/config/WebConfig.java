package org.cqipc.edu.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Description: SpringMVC配置
 * @Author: Heyue
 * @Date: 2020/7/2
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {


    /**
     * @Description: 添加静态资源处理
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/api/*");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/common/*");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/css/*");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/images/*");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/js/*");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/lib/*");
    }

    /**
     * @Description: 添加跨域映射
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                .maxAge(3600);
    }
}
