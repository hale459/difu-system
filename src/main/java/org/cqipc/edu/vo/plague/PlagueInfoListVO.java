package org.cqipc.edu.vo.plague;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 瘟疫情况表格VO
 * @ClassName: PlagueInfoListVO
 * @Author: Heyue
 * @DateTime: 2020-06-26
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class PlagueInfoListVO implements Serializable {

    private Integer plagueInfoId;//主键id
    private String plagueName;//瘟疫ID
    private String areaName;//发生地区ID
    private String happenTime;//发生时间
    private String info;//描述
    private String name;//感染体姓名
    private Integer number;//感染人数
    private Integer dienumber;//死亡人数
    private Integer uid;//感染体ID

}
