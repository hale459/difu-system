package org.cqipc.edu.vo.plague;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 散布瘟疫返回信息VO
 * @ClassName: plagueVo
 * @Author: Heyue
 * @DateTime: 2020-06-26
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class plagueVo implements Serializable {

    private Integer areaCountNum;//地区总人数
    private Integer infectedNum;//被感染人数
    private Integer dieCountNum;//总死亡人数
    private Integer statusCode;//返回状态码
    private String msg;//返回结果

}
