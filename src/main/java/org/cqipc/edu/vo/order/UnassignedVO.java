package org.cqipc.edu.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 未分配(勾魂)用户VO
 * @ClassName: UnassignedVO
 * @Author: Heyue
 * @DateTime: 2020-06-21
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UnassignedVO implements Serializable {

    private Integer lifeId;//生薄ID(主键)
    private String lifeName;//生薄用户名
    private String lifeSex;//生薄性别(0:男  1:女  2:保密)
    private Integer lifeCountAge;//总寿命
    private Integer lifeCurrentAge;//当前寿命
    private String lifeType;//类型
    private String lifeArea;//地区
    private String lifeReincarnation;//前世
    private Integer lifeStatu;//状态(1:生  2:死)
    private String lifeDesc;//描述
    private String lifeCreatdate;//创建时间
    private String lifeLassTime;//最后编辑时间
    private String lifeDieTime;//死亡时间
    private String lifeDieWhy;//死亡原因

}
