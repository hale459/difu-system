package org.cqipc.edu.vo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 用户列表VO类
 * @ClassName: UserListVO
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserListVO implements Serializable {

    private Integer id;
    private String username;
    private Integer age;
    private String sex;
    private String email;
    private String lastLoginTime;
    private String description;

}
