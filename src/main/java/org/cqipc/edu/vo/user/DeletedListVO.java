package org.cqipc.edu.vo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 已删除用户VO
 * @ClassName: DeletedListVO
 * @Author: Heyue
 * @DateTime: 2020-06-24
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DeletedListVO implements Serializable {

    private Integer delId;//ID(主键)
    private String delName;//已删除用户名
    private String delSex;//性别
    private Integer delAge;//死时寿命
    private String delWhy;//死亡原因
    private Integer delReinState;//轮回状态(1:未轮回  2:已轮回)
    private String delTime;//死亡时间
    private String delLassTime;//最后编辑时间

}
