package org.cqipc.edu.vo.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 生薄修改用户VO
 * @ClassName: UserlifeModifyVO
 * @Author: Heyue
 * @DateTime: 2020-06-22
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserlifeModifyVO implements Serializable {

    private Integer id;
    private String name;
    private String sex;
    private Integer currenAge;
    private Integer overAge;
    private Integer type;

}
