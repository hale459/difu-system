package org.cqipc.edu.vo.book;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.annotation.security.DenyAll;
import java.io.Serializable;

/**
 * @Description: 分页查询所有生薄信息VO
 * @ClassName: UserlifeListVO
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Data
@DenyAll
@NoArgsConstructor
@Accessors(chain = true)
public class UserlifeListVO implements Serializable {

    private Integer id;
    private String username;
    private String sex;
    private Integer totalAge;
    private Integer overAge;
    private String type;
    private String lass_updatetime;

}
