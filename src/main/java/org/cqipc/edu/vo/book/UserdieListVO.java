package org.cqipc.edu.vo.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 死薄用户分页查询VO
 * @ClassName: UserdieListVO
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserdieListVO implements Serializable {

    private Integer id;
    private String username;
    private Integer totalAge;
    private String dieType;
    private String type;
    private String lass_updatetime;
}
