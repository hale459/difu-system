package org.cqipc.edu.vo.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 死薄用户修改VO
 * @ClassName: UserdieModifyVO
 * @Author: Heyue
 * @DateTime: 2020-06-23
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserdieModifyVO implements Serializable {

    private Integer dieId;
    private String username;
    private Integer dieType;
    private Integer dieWhy;

}
