package org.cqipc.edu.vo.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Description: 添加生薄用户VO
 * @ClassName: UserlifeAddVO
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/


@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserlifeAddVO {

    private String lifeName;
    private String lifeSex;
    private Integer lifeCountAge;
    private Integer lifeType;
    private Integer lifeArea;
    private String lifeDesc;
    private String reincarnation;//前世

}
