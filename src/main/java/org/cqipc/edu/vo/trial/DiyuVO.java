package org.cqipc.edu.vo.trial;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 地狱VO
 * @ClassName: DiyuVO
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DiyuVO implements Serializable {

    private Integer dycj;
    private String year;
    private Integer userID;
    private String userName;
    private String info;

}
