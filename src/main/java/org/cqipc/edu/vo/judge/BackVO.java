package org.cqipc.edu.vo.judge;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 退单VO
 * @ClassName: BackVO
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class BackVO implements Serializable {

    private String info;

}
