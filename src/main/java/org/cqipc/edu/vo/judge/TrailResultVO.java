package org.cqipc.edu.vo.judge;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Description: 添加审判记录VO
 * @ClassName: TrailResultVO
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TrailResultVO {

    private Integer id;//主键ID
    private Integer userId;//被执行用户ID
    private String userName;//被执行用户姓名
    private String info;//审判描述

}
