package org.cqipc.edu.vo.judge;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 待审判表格VO
 * @ClassName: UntrialTableVO
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UntrialTableVO implements Serializable {

    private Integer fId;//分配表ID
    private Integer fFid;//被分配人的ID
    private String fName;//被分配人的姓名
    private String fSex;//生薄性别(0:男  1:女  2:保密)
    private Integer fCountAge;//总寿命
    private Integer fCurrentAge;//当前寿命
    private String fType;//类型 改
    private String fArea;//地区 改
    private String fReincarnation;//前世
    private Integer fStatu;//状态(1:生  2:死)
    private String fDesc;//描述
    private String fCreatdate;//创建时间
    private String fLassTime;//最后编辑时间
    private String fDieTime;//死亡时间
    private String fDieWhy;//死亡原因 改
    private Integer fFprid;//分配人ID
    private String fFpr;//分配人姓名
    private String fTime;//分配时间
    private Integer fStatus;//勾魂状态
    private Integer spzt;//审判状态
    private Integer types;//审判类型 0 1 2

}
