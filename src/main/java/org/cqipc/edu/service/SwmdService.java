package org.cqipc.edu.service;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Swmd;

import java.util.List;

/**
 * @Description: 死亡名单-服务接口
 * @ClassName: SwmdService
 * @Author: Heyue
 * @DateTime: 2020-06-26
 **/
public interface SwmdService {

    //批量添加死亡名单
    Object batchAddSwmd(List<Swmd> swmd);

    //分页查询死亡名单
    Object getSwmdList(@Param("page") Pagination pagination, @Param("id") Integer id);

}
