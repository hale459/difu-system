package org.cqipc.edu.service;

import org.cqipc.edu.entity.Pagination;

import java.util.List;

/**
 * @Description: 刑罚-服务接口
 * @ClassName: XfService
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
public interface XfService {

    //删除
    Object updateXf(Integer id);

    //获取所有刑法现场
    Object getXfxcList(Pagination pagination, String username, Integer level);

    //获取所以有刑罚记录
    Object getXfjlList(Pagination pagination, String username, Integer statu);

    //批量删除
    Object batchLog(List<Integer> stringToIntegerlIST);

    //通过ID删除
    Object deleteLogById(Integer tableId);
}
