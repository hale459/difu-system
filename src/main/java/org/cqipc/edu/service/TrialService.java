package org.cqipc.edu.service;

/**
 * @Description: 审判记录-服务接口
 * @ClassName: TrialService
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
public interface TrialService {

    //批量添加审判记录
    Object addTrial(Integer id);

}
