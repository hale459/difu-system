package org.cqipc.edu.service;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Userlife;
import org.cqipc.edu.vo.book.UserlifeAddVO;
import org.cqipc.edu.vo.book.UserlifeModifyVO;

import java.util.List;

/**
 * @Description: 生薄用户-服务接口
 * @ClassName: UserlifeService
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface UserlifeService {

    //添加生薄用户
    Object LivebookAdd(UserlifeAddVO userlifeAddVO, Integer mid);

    //分页/条件查询生薄信息
    Object getLivebookList(Pagination pagination, String username, Integer type);

    //通过生薄ID删除用户信息
    Object removeUserlifeById(Integer id);

    //根据生薄用户ID查询信息
    Userlife getUserlifeById(Integer id);

    //修改用户用户信息
    Object modifyUserlife(UserlifeModifyVO modifyVO);

    //生薄批量删除
    Object batchDelLive(List<Integer> item);

    //查询所有生薄信息
    Object getUserlifeAll();

    //根据地区ID查询用户信息
    Object getUserlifeByAreaId(Integer id);

}
