package org.cqipc.edu.service;

/**
 * @Description: 部门-服务接口
 * @ClassName: DeptService
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
public interface DeptService {

    //获取部门信息
    Object getDeptList();

}
