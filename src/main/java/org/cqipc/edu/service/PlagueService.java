package org.cqipc.edu.service;

/**
 * @Description: 瘟疫种类-服务接口
 * @ClassName: PlagueService
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
public interface PlagueService {

    //查询所有瘟疫信息
    Object getPlagueAll();

    //根据ID查询瘟疫信息
    Object getPlagueById(Integer id);

    //根据瘟疫名称查询瘟疫信息
    Object getPlagueByName(String name);

    //发布瘟疫处理
    Object sendPlague(Integer plague, Integer area, Integer userlife, String description);

}
