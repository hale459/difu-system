package org.cqipc.edu.service;

import org.cqipc.edu.entity.Pagination;

import java.util.List;

/**
 * @Description: 已删除用户-服务接口
 * @ClassName: DeletedService
 * @Author: Heyue
 * @DateTime: 2020-06-24
 **/
public interface DeletedService {

    //分页/条件查询所有已删除用户信息
    Object getDeletedList(Pagination pagination, Integer id, String username);

    //批量删除已删除用户
    Object batchDelDeleted(List<Integer> item);

    //通过ID删除已删除用户信息(永久删除, 无法轮回)
    Object removeDeletedById(Integer id);

    //根据ID拿到已删除用户的信息
    Object getDeletedById(Integer id);

    //退回死薄
    Object backDiebook(Integer id);
}
