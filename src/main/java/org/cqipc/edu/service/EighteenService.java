package org.cqipc.edu.service;

/**
 * @Description: 地狱管理-服务接口
 * @ClassName: EighteenService
 * @Author: Heyue
 * @DateTime: 2020-06-29
 **/
public interface EighteenService {

    //获取全部地狱层级信息
    Object getEighteenLevel();

}
