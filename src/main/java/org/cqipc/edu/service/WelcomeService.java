package org.cqipc.edu.service;

/**
 * @Description: 欢迎页-服务接口
 * @ClassName: WelcomeService
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
public interface WelcomeService {

    //返回首页显示的数据
    Object WelcomeData();

}
