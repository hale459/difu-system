package org.cqipc.edu.service;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.vo.book.UserdieModifyVO;

import java.util.List;

/**
 * @Description: 死薄用户-服务接口
 * @ClassName: UserdieService
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface UserdieService {

    //分页/条件查询死薄信息
    Object getDiebookList(Pagination pagination, String username, Integer type, Integer dieWhy);

    //根据死薄用户ID信息查询信息
    Object getUserdieById(Integer id);

    //修改死薄用户信息
    Object modifyUserdie(UserdieModifyVO modifyVO);

    //通过死薄ID删除用户信息
    Object removeUserdieById(Integer id);

    //死薄批量删除
    Object batchDelDie(List<Integer> item);
}
