package org.cqipc.edu.service.impl;

import org.cqipc.edu.dao.EighteenDao;
import org.cqipc.edu.service.EighteenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: 地狱管理-服务实现
 * @ClassName: EighteenServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-29
 **/
@Service
@Transactional
public class EighteenServiceImpl implements EighteenService {

    @Autowired
    private EighteenDao eighteenDao;

    /**
     * @Description: 获取所有地狱层级信息
     * @Author: Heyue
     * @Date: 2020/6/29
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getEighteenLevel() {
        return eighteenDao.getEighteenLevel();
    }
}
