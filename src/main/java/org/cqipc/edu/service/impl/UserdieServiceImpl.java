package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.Constants;
import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.common.TypeResult;
import org.cqipc.edu.dao.DeletedDao;
import org.cqipc.edu.dao.DiewhyDao;
import org.cqipc.edu.dao.TypeDao;
import org.cqipc.edu.dao.UserdieDao;
import org.cqipc.edu.entity.Deleted;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Userdie;
import org.cqipc.edu.service.UserdieService;
import org.cqipc.edu.vo.book.UserdieListVO;
import org.cqipc.edu.vo.book.UserdieModifyVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 死薄用户-接口实现
 * @ClassName: UserdieServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Service
@Transactional
public class UserdieServiceImpl implements UserdieService {

    @Autowired
    private UserdieDao userdieDao;

    @Autowired
    private DiewhyDao diewhyDao;

    @Autowired
    private TypeDao typeDao;

    @Autowired
    private DeletedDao deletedDao;


    /**
     * @Description: 分页查询/条件查询死薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getDiebookList(Pagination pagination, String username, Integer type, Integer dieWhy) {
        ResultTable resultTable = new ResultTable();
        List<UserdieListVO> list = new ArrayList<>();
        List<Userdie> diebookList = userdieDao.getDiebookList(pagination, username, type, dieWhy);
        for (Userdie userdie : diebookList) {
            UserdieListVO vo = new UserdieListVO();
            vo.setId(userdie.getDieId())
                    .setUsername(userdie.getDieName())
                    .setTotalAge(userdie.getDieAge())
                    .setDieType(diewhyDao.getDiewhyById(userdie.getDieWhy()).getName())
                    .setType(typeDao.getTypeById(userdie.getDieType()).getTypeName())
                    .setLass_updatetime(userdie.getDieLassTime());
            list.add(vo);
        }
        resultTable.setCode(StatusConstant.SUCCESS)
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setCount(userdieDao.getCountPage(username, type, dieWhy).size())
                .setData(list);
        return resultTable;
    }


    /**
     * @Description: 根据死薄ID获取信息并返回死亡原因和类型(编号)
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getUserdieById(Integer id) {
        Userdie userdie = userdieDao.getUserdieById(id);
        TypeResult typeResult = new TypeResult();
        return typeResult.setDieType(userdie.getDieType())
                .setDieWhy(userdie.getDieWhy());
    }


    /**
     * @Description: 修改死薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object modifyUserdie(UserdieModifyVO modifyVO) {
        Userdie oldData = userdieDao.getUserdieById(modifyVO.getDieId());
        Userdie newData = new Userdie();
        BeanUtils.copyProperties(oldData, newData);
        newData.setDieName(modifyVO.getUsername())
                .setDieType(modifyVO.getDieType())
                .setDieLassTime(Constants.CURRENT_TIME)
                .setDieWhy(modifyVO.getDieWhy());
        return userdieDao.modifyUserdie(newData) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 根据死薄的ID删除死薄用户信息并添加到已删除用户表中
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object removeUserdieById(Integer id) {
        Deleted deleted = new Deleted();
        Userdie userdie = userdieDao.getUserdieById(id);
        deleted.setDelDieId(userdie.getDieId())
                .setDelName(userdie.getDieName())
                .setDelSex(userdie.getDieSex())
                .setDelCountAge(userdie.getDieCountAge())
                .setDelAge(userdie.getDieAge())
                .setDelType(userdie.getDieType())
                .setDelWhy(userdie.getDieWhy())
                .setDelArea(userdie.getDieArea())
                .setDelReincarnation(userdie.getDieReincarnation())
                .setDelReinState(userdie.getDieReinState())
                .setDelStatu(userdie.getDieStatu())
                .setDelTime(userdie.getDieTime())
                .setDelDesc(userdie.getDieDesc())
                .setDelCreatdate(userdie.getDieCreatdate())
                .setDelLassTime(userdie.getDieLassTime())
                .setDelTimeDel(Constants.CURRENT_TIME);
        return userdieDao.removeUserdieById(id) > 0 && deletedDao.deletedAdd(deleted) > 0 ? StatusConstant.MSG_SUCCESS :
                StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 死薄批量删除
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchDelDie(List<Integer> item) {
        for (Integer userdie : item) {
            Deleted deleted = new Deleted();
            Userdie userdieInfo = userdieDao.getUserdieById(userdie);
            deleted.setDelDieId(userdieInfo.getDieId())
                    .setDelName(userdieInfo.getDieName())
                    .setDelSex(userdieInfo.getDieSex())
                    .setDelCountAge(userdieInfo.getDieCountAge())
                    .setDelAge(userdieInfo.getDieAge())
                    .setDelType(userdieInfo.getDieType())
                    .setDelWhy(userdieInfo.getDieWhy())
                    .setDelArea(userdieInfo.getDieArea())
                    .setDelReincarnation(userdieInfo.getDieReincarnation())
                    .setDelReinState(userdieInfo.getDieReinState())
                    .setDelStatu(userdieInfo.getDieStatu())
                    .setDelTime(userdieInfo.getDieTime())
                    .setDelDesc(userdieInfo.getDieDesc())
                    .setDelCreatdate(userdieInfo.getDieCreatdate())
                    .setDelLassTime(userdieInfo.getDieLassTime())
                    .setDelTimeDel(Constants.CURRENT_TIME);
            deletedDao.deletedAdd(deleted);
        }
        return userdieDao.batchDelDie(item) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }
}
