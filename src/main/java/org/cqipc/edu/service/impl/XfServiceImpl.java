package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.Constants;
import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.XfjlDao;
import org.cqipc.edu.dao.XfxcDao;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Xfjl;
import org.cqipc.edu.service.XfService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 刑罚-服务实现
 * @ClassName: XfServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
@Controller
@Transactional
public class XfServiceImpl implements XfService {

    @Autowired
    private XfxcDao xfxcDao;

    @Autowired
    private XfjlDao xfjlDao;


    /**
     * @Description: 提前释放
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object updateXf(Integer id) {
        boolean flag = false;
        Xfjl xfjl = new Xfjl();
        //添加到刑罚记录
        BeanUtils.copyProperties(xfxcDao.getXfxcById(id), xfjl);
        xfjl.setXId(null)
                .setXOuttime(Constants.CURRENT_DATE)
                .setXStatu(1);
        if (xfjlDao.addXfjl(xfjl) > 0) {
            if (xfxcDao.delById(id) > 0) {
                flag = true;
            }
        }
        return flag ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }

    /**
     * @Description: 获取所有
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getXfxcList(Pagination pagination, String username, Integer level) {
        ResultTable resultTable = new ResultTable();
        return resultTable.setCount(xfxcDao.getXfxcListCount(username, level).size())
                .setData(xfxcDao.getXfxcList(pagination, username, level))
                .setCode(StatusConstant.SUCCESS)
                .setMsg(StatusConstant.MSG_DEFAULT);
    }

    /**
     * @Description: 查询刑罚记录
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getXfjlList(Pagination pagination, String username, Integer statu) {
        ResultTable resultTable = new ResultTable();
        return resultTable.setCount(xfjlDao.getXfjlListCount(username, statu).size())
                .setData(xfjlDao.getXfjlList(pagination, username, statu))
                .setCode(StatusConstant.SUCCESS)
                .setMsg(StatusConstant.MSG_DEFAULT);
    }


    /**
     * @Description: 通过ID删除
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchLog(List<Integer> ids) {
        return xfjlDao.batchDelXfjl(ids) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }

    /**
     * @Description: 批量删除
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object deleteLogById(Integer tableId) {
        return xfjlDao.removeCfjlById(tableId) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }
}
