package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.WelcomeData;
import org.cqipc.edu.dao.DspDao;
import org.cqipc.edu.dao.UserdieDao;
import org.cqipc.edu.dao.UserlifeDao;
import org.cqipc.edu.service.WelcomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: 欢迎页-接口实现
 * @ClassName: WelcomeServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Service
@Transactional
public class WelcomeServiceImpl implements WelcomeService {

    @Autowired
    private UserlifeDao userlifeDao;

    @Autowired
    private UserdieDao userdieDao;

    @Autowired
    private DspDao dspDao;




    /**
     * @Description: 返回欢迎页需要的数据
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @Override
    public Object WelcomeData() {
        WelcomeData data = new WelcomeData();
        data.setLifeCount(userlifeDao.getLivebookAll().size())
                .setDieCount(userdieDao.getUserdieAll().size())
                .setDfpCount(userlifeDao.getUnassignedAll().size())
                .setDspCount(dspDao.getDspAll().size());
        return data;
    }
}
