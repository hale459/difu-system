package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.DistrictDao;
import org.cqipc.edu.dao.PlagueDao;
import org.cqipc.edu.dao.PlagueInfoDao;
import org.cqipc.edu.dao.SwmdDao;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Plague_Info;
import org.cqipc.edu.entity.Swmd;
import org.cqipc.edu.service.PlagueInfoService;
import org.cqipc.edu.vo.plague.PlagueInfoListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:瘟疫情况-接口实现
 * @ClassName: PlagueInfoServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
@Service
@Transactional
public class PlagueInfoServiceImpl implements PlagueInfoService {

    @Autowired
    private PlagueInfoDao plagueInfoDao;

    @Autowired
    private DistrictDao districtDao;

    @Autowired
    private PlagueDao plagueDao;

    @Autowired
    private SwmdDao swmdDao;

    /**
     * @Description: 添加瘟疫情况信息
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object addPlagueInfo(Plague_Info plagueInfo) {
        return plagueInfoDao.addPlagueInfo(plagueInfo) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 分页/条件查询瘟疫情况
     * @Author: Heyue
     * @Date: 2020/6/26
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getPlagueInfoList(Pagination pagination, String username, Integer plague, Integer area) {
        List<PlagueInfoListVO> list = new ArrayList<>();
        for (Plague_Info info : plagueInfoDao.getPlagueInfoList(pagination, username, plague, area)) {
            PlagueInfoListVO vo = new PlagueInfoListVO();
            BeanUtils.copyProperties(info, vo);
            vo.setAreaName(districtDao.getDistrictById(info.getAreaId()).getDistrictName())
                    .setPlagueName(plagueDao.getPlagueById(info.getPlagueId()).getPlagueName());
            list.add(vo);
        }
        ResultTable resultTable = new ResultTable();
        resultTable.setData(list)
                .setCount(plagueInfoDao.getPlagueInfoCount(username, plague, area).size())
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setCode(StatusConstant.SUCCESS);
        return resultTable;
    }


    /**
     * @Description: 批量删除瘟疫情况
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchDelPlagueInfo(List<Integer> id) {
        List<Integer> swmdId = new ArrayList<>();
        for (Integer info : id) {
            for (Swmd swmd : swmdDao.getPlaugeInfoByFzid(plagueInfoDao.getPlaugeInfoById(info).getUid())) {
                swmdId.add(swmd.getId());
            }
        }
        plagueInfoDao.batchDelPlagueInfo(id);
        return swmdDao.batchDelSwmd(swmdId) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 通过ID删除瘟疫信息
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object removePlaugeInfoByid(Integer id) {
        List<Integer> swmdId = new ArrayList<>();
        for (Swmd swmd : swmdDao.getPlaugeInfoByFzid(plagueInfoDao.getPlaugeInfoById(id).getUid())) {
            swmdId.add(swmd.getId());
        }
        plagueInfoDao.removePlaugeInfoByid(id);
        return swmdDao.batchDelSwmd(swmdId) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 通过瘟疫情况ID
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getPlaugeInfoById(Integer id) {
        return plagueInfoDao.getPlaugeInfoById(id);
    }
}
