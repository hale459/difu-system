package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.DiewhyDao;
import org.cqipc.edu.dao.DistrictDao;
import org.cqipc.edu.dao.MengpoDao;
import org.cqipc.edu.dao.TypeDao;
import org.cqipc.edu.entity.Mengpo;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.MengpoService;
import org.cqipc.edu.vo.mengpo.MengpoTableVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 孟婆管理-服务实现
 * @ClassName: MengpoServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-29
 **/
@Service
@Transactional
public class MengpoServiceImpl implements MengpoService {

    @Autowired
    private MengpoDao mengpoDao;

    @Autowired
    private DistrictDao districtDao;

    @Autowired
    private TypeDao typeDao;

    @Autowired
    private DiewhyDao diewhyDao;

    /**
     * @Description: 分页/条件获取孟婆信息
     * @Author: Heyue
     * @Date: 2020/6/29
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getMengpoAll(Pagination pagination, Integer diewhy, String username) {
        List<MengpoTableVO> list = new ArrayList<>();
        for (Mengpo mengpo : mengpoDao.getMengpoPage(pagination, diewhy, username)) {
            MengpoTableVO vo = new MengpoTableVO();
            BeanUtils.copyProperties(mengpo, vo);
            vo.setFType(typeDao.getTypeById(mengpo.getFType()).getTypeName())
                    .setFArea(districtDao.getDistrictById(mengpo.getFArea()).getDistrictName())
                    .setFDieWhy(diewhyDao.getDiewhyById(mengpo.getFDieWhy()).getName());
            list.add(vo);
        }

        ResultTable resultTable = new ResultTable();
        return resultTable.setData(list)
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setCode(StatusConstant.SUCCESS)
                .setCount(mengpoDao.getMengpoPageCount(diewhy, username).size());
    }
}
