package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.Constants;
import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.*;
import org.cqipc.edu.entity.*;
import org.cqipc.edu.service.OrderService;
import org.cqipc.edu.vo.order.FpeiVO;
import org.cqipc.edu.vo.order.UnassignedVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 订单管理-接口实现
 * @ClassName: OrderServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-27
 **/
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private UserlifeDao userlifeDao;

    @Autowired
    private DiewhyDao diewhyDao;

    @Autowired
    private DistrictDao districtDao;

    @Autowired
    private TypeDao typeDao;

    @Autowired
    private UserdieDao userdieDao;

    @Autowired
    private FpeiDao fpeiDao;

    @Autowired
    private DspDao dspDao;


    /**
     * @Description: 获取死亡后未分配的人
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getUnassigned(Pagination pagination, String username, String date, Integer statu) {
        List<UnassignedVO> list = new ArrayList<>();
        for (Userlife userlife : userlifeDao.getUnassigned(pagination, username, date, statu)) {
            UnassignedVO vo = new UnassignedVO();
            BeanUtils.copyProperties(userlife, vo);
            vo.setLifeDieWhy(diewhyDao.getDiewhyById(userlife.getLifeDieWhy()).getName())
                    .setLifeArea(districtDao.getDistrictById(userlife.getLifeArea()).getDistrictName())
                    .setLifeType(typeDao.getTypeById(userlife.getLifeType()).getTypeName());
            list.add(vo);
        }
        ResultTable resultTable = new ResultTable();
        resultTable.setCount(userlifeDao.getUnassignedPage(username, date, statu).size())
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setCode(StatusConstant.SUCCESS)
                .setData(list);
        return resultTable;
    }


    /**
     * @Description: 批量分配
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchUnassigned(List<Integer> id, HttpSession session) {
        /*
        1.将传回的ID对应的每一个生薄中的信息放到分配表中去
        2.批量删除生薄中已经死亡的人
        3.放在分配信息中的所有人批量添加到分配表中
         */
        List<Fpei> fps = new ArrayList<>();
        for (Integer i : id) {
            Fpei fp = new Fpei();
            Userlife userlife = userlifeDao.getUserlifeById(i);
            fp.setFFid(userlife.getLifeId())
                    .setFName(userlife.getLifeName())
                    .setFSex(userlife.getLifeSex())
                    .setFCountAge(userlife.getLifeCountAge())
                    .setFCurrentAge(userlife.getLifeCurrentAge())
                    .setFType(userlife.getLifeType())
                    .setFArea(userlife.getLifeArea())
                    .setFReincarnation(userlife.getLifeReincarnation())
                    .setFStatu(userlife.getLifeStatu())
                    .setFDesc(userlife.getLifeDesc())
                    .setFCreatdate(Constants.CURRENT_TIME)
                    .setFLassTime(Constants.CURRENT_TIME)
                    .setFDieTime(userlife.getLifeDieTime())
                    .setFDieWhy(userlife.getLifeDieWhy())
                    .setFFprid(((User) session.getAttribute("UserInfo")).getUserId().intValue())
                    .setFFpr(((User) session.getAttribute("UserInfo")).getUsername())
                    .setFTime(Constants.CURRENT_TIME)
                    .setFStatus(Constants.USERDIE_STATE_FALSE);
            fps.add(fp);
        }
        fpeiDao.batchAddFpei(fps);
        return userlifeDao.batchDelLive(id) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 通过ID分配
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object deleteUnassignedById(Integer id, HttpSession session) {
        List<Fpei> fps = new ArrayList<>();
        List<Dsp> dsps = new ArrayList<>();
        Fpei fp = new Fpei();
        Dsp dsp = new Dsp();
        Userlife userlife = userlifeDao.getUserlifeById(id);
        fp.setFFid(userlife.getLifeId())
                .setFName(userlife.getLifeName())
                .setFSex(userlife.getLifeSex())
                .setFCountAge(userlife.getLifeCountAge())
                .setFCurrentAge(userlife.getLifeCurrentAge())
                .setFType(userlife.getLifeType())
                .setFArea(userlife.getLifeArea())
                .setFReincarnation(userlife.getLifeReincarnation())
                .setFStatu(userlife.getLifeStatu())
                .setFDesc(userlife.getLifeDesc())
                .setFCreatdate(Constants.CURRENT_TIME)
                .setFLassTime(Constants.CURRENT_TIME)
                .setFDieTime(userlife.getLifeDieTime())
                .setFDieWhy(userlife.getLifeDieWhy())
                .setFFprid(((User) session.getAttribute("UserInfo")).getUserId().intValue())
                .setFFpr(((User) session.getAttribute("UserInfo")).getUsername())
                .setFTime(Constants.CURRENT_TIME)
                .setFStatus(Constants.USERDIE_STATE_FALSE);
        BeanUtils.copyProperties(fp, dsp);
        dsp.setSpzt(1);//修改审判状态
        dsps.add(dsp);
        dspDao.batchAddDsp(dsps);
        fps.add(fp);
        fpeiDao.batchAddFpei(fps);
        return userlifeDao.removeUserlifeById(id) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 分页/条件查询已分配的人
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getFpeiPage(Pagination pagination, Integer statu, String username) {
        List<FpeiVO> list = new ArrayList<>();
        for (Fpei f : fpeiDao.getFpeiPage(pagination, statu, username)) {
            FpeiVO vo = new FpeiVO();
            BeanUtils.copyProperties(f, vo);
            vo.setFArea(districtDao.getDistrictById(f.getFArea()).getDistrictName())
                    .setFType(typeDao.getTypeById(f.getFType()).getTypeName())
                    .setFDieWhy(diewhyDao.getDiewhyById(f.getFDieWhy()).getName());
            list.add(vo);
        }
        ResultTable resultTable = new ResultTable();
        resultTable.setData(list)
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setCode(StatusConstant.SUCCESS)
                .setCount(fpeiDao.getFpeiCount(statu, username).size());
        return resultTable;
    }


    /**
     * @Description: 批量勾魂
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchGouhun(List<Integer> ids) {
        /*
        1.向审判表批量添加对应ID的数据
        2.将分配表记录中的分配状态改为已分配
         */
        boolean flag = false;
        List<Fpei> fps = new ArrayList<>();
        List<Dsp> list = new ArrayList<>();
        for (Integer id : ids) {
            Fpei fp = new Fpei();
            Dsp dsp = new Dsp();
            Fpei fpeiById = fpeiDao.getFpeiById(id);
            BeanUtils.copyProperties(fpeiById, dsp);
            fp.setFStatus(Constants.USERDIE_STATE_TRUE)
                    .setFId(id);
            dsp.setFStatus(Constants.USERDIE_STATE_TRUE)
                    .setSpzt(1);
            list.add(dsp);
            fps.add(fp);
        }
        if (dspDao.batchAddDsp(list) > 0 && fpeiDao.batchModByStatu(fps) > 0) {
            flag = true;
        }
        return flag ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 勾魂操作
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object removeFpeiById(Integer id) {
        boolean flag = false;
        List<Fpei> fps = new ArrayList<>();
        List<Dsp> list = new ArrayList<>();
        Fpei fp = new Fpei();
        Dsp dsp = new Dsp();
        Fpei fpeiById = fpeiDao.getFpeiById(id);
        BeanUtils.copyProperties(fpeiById, dsp);
        fp.setFStatus(Constants.USERDIE_STATE_TRUE)
                .setFId(id);
        dsp.setFStatus(Constants.USERDIE_STATE_TRUE)
                .setSpzt(1);
        list.add(dsp);
        fps.add(fp);
        if (dspDao.batchAddDsp(list) > 0 && fpeiDao.batchModByStatu(fps) > 0) {
            flag = true;
        }
        return flag ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 通过ID删除
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object delFpeiById(Integer id) {
        return fpeiDao.delFpeiById(id) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }
}

