package org.cqipc.edu.service.impl;

import org.cqipc.edu.dao.DiewhyDao;
import org.cqipc.edu.service.DiewhyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: 死亡原因-接口实现
 * @ClassName: DiewhyServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-21
 **/
@Service
@Transactional
public class DiewhyServiceImpl implements DiewhyService {

    @Autowired
    private DiewhyDao diewhyDao;


    /**
     * @Description: 查询所有死亡原因
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getDiewhyAll() {
        return diewhyDao.getDiewhyAll();
    }


    /**
     * @Description: 根据死亡原因ID拿到死亡数据
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    public Object getDiewhyById(Integer id) {
        return diewhyDao.getDiewhyById(id);
    }

}
