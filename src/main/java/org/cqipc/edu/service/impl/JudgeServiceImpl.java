package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.Constants;
import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.*;
import org.cqipc.edu.entity.*;
import org.cqipc.edu.service.JudgeService;
import org.cqipc.edu.vo.judge.BackVO;
import org.cqipc.edu.vo.judge.UntrialTableVO;
import org.cqipc.edu.vo.trial.DiyuVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 审判-接口实现
 * @ClassName: JudgeServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Service
@Transactional
public class JudgeServiceImpl implements JudgeService {

    @Autowired
    private DspDao dspDao;

    @Autowired
    private TypeDao typeDao;

    @Autowired
    private UserdieDao userdieDao;

    @Autowired
    private DistrictDao districtDao;

    @Autowired
    private UserlifeDao userlifeDao;

    @Autowired
    private DiewhyDao diewhyDao;

    @Autowired
    private TrialDao trialDao;

    @Autowired
    private MengpoDao mengpoDao;

    @Autowired
    private XfxcDao xfxcDao;

    /**
     * @Description: 分页/条件获取【待审判】表格数据
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getDspList(Pagination pagination, String username, String date) {
        List<UntrialTableVO> list = new ArrayList<>();
        for (Dsp dsp : dspDao.getDspList(pagination, username, date, 1)) {
            UntrialTableVO vo = new UntrialTableVO();
            BeanUtils.copyProperties(dsp, vo);
            vo.setFType(typeDao.getTypeById(dsp.getFType()).getTypeName())
                    .setFArea(districtDao.getDistrictById(dsp.getFArea()).getDistrictName())
                    .setFDieWhy(diewhyDao.getDiewhyById(dsp.getFDieWhy()).getName());
            list.add(vo);
        }
        ResultTable resultTable = new ResultTable();
        resultTable.setCount(dspDao.getDspListCount(username, date, 1).size())
                .setCode(StatusConstant.SUCCESS)
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setData(list);
        return resultTable;
    }


    /**
     * @Description: 分页/条件获取【已审判】表格数据
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getDspdList(Pagination pagination, String username, String date) {
        List<UntrialTableVO> list = new ArrayList<>();
        for (Dsp dsp : dspDao.getDspList(pagination, username, date, 2)) {
            UntrialTableVO vo = new UntrialTableVO();
            BeanUtils.copyProperties(dsp, vo);
            vo.setFType(typeDao.getTypeById(dsp.getFType()).getTypeName())
                    .setFArea(districtDao.getDistrictById(dsp.getFArea()).getDistrictName())
                    .setFDieWhy(diewhyDao.getDiewhyById(dsp.getFDieWhy()).getName())
                    .setTypes(trialDao.getTrialByUserId(dsp.getFFid()).getTypes());
            list.add(vo);
        }
        ResultTable resultTable = new ResultTable();
        resultTable.setCount(dspDao.getDspListCount(username, date, 2).size())
                .setCode(StatusConstant.SUCCESS)
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setData(list);
        return resultTable;
    }


    /**
     * @Description: 通过ID珊瑚【待审判】表中已经被审判的人
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object delDspById(Integer id) {
        return dspDao.delDspById(id) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 批量删除【待审判记录】
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchDsp(List<Integer> ids) {
        return dspDao.batchDelDsp(ids) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }

    /**
     * @Description: 退单操作(事件处理)
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object backOrder(BackVO backVO, Integer id, HttpSession session) {
        /*
        1.通过ID拿到待审判用户信息
        2.把数据添加到生薄信息中
        3.添加审判记录
         */
        boolean flag = false;
        Userlife userlife = new Userlife();
        Dsp dspById = dspDao.getDspById(id);
        userlife.setLifeId(dspById.getFFid())
                .setLifeName(dspById.getFName())
                .setLifeSex(dspById.getFSex())
                .setLifeCountAge(dspById.getFCountAge())
                .setLifeCurrentAge(dspById.getFCurrentAge())
                .setLifeType(dspById.getFType())
                .setLifeArea(dspById.getFArea())
                .setLifeReincarnation(dspById.getFReincarnation())
                .setLifeStatu(dspById.getFStatu())
                .setLifeDesc(dspById.getFDesc())
                .setLifeCreatdate(dspById.getFCreatdate())
                .setLifeLassTime(Constants.CURRENT_TIME);
        Mingjie_Trial trial = new Mingjie_Trial();
        trial.setInfo(backVO.getInfo())
                .setTrialTime(Constants.CURRENT_TIME)
                .setTrialUserId(((User) session.getAttribute("UserInfo")).getUserId().intValue())
                .setUserId(dspById.getFFid())
                .setUserName(dspById.getFName())
                .setType(Constants.USERDIE_STATE_TRUE)
                .setTypes(0);
        //执行操作
        if (userlifeDao.LivebookAdd(userlife) > 0) {
            if (dspDao.updateDspStatu(2, id) > 0) {
                if (trialDao.addTrial(trial) > 0) {
                    flag = true;
                }
            }
        }
        return flag ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }

    /**
     * @Description: 获取审判记录
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getInfoById(Integer id) {
        return trialDao.getTrialByUserId(id);
    }


    /**
     * @Description: 安排投胎(事件处理)
     * @Author: Heyue
     * @Date: 2020/6/29
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object Reincarnation(BackVO backVO, Integer id, HttpSession session) {
        /*
            1.传回：审问记录和待审判主键ID
            2.通过ID查询待审判记录，将信息添加到t_mengpo表中，参数无需改动，直接copy就行(添加t_mengpo)
            3.通过ID修改待审判表中的spzt为2，也就是已审判状态(修改t_dsp)
            4.将信息添加到死薄
            5.将信息添加到审判记录中
            6.将数据添加到孟婆表中后，由孟婆安排投胎
         */
        boolean flag = false;
        Mengpo mengpo = new Mengpo();
        Userdie userdie = new Userdie();
        Mingjie_Trial trial = new Mingjie_Trial();
        //通过ID拿到数据
        Dsp dsp = dspDao.getDspById(id);
        //复制数据到孟婆记录(未轮回)
        BeanUtils.copyProperties(dsp, mengpo);
        mengpo.setFStatus(2)
                .setSpzt(2)
                .setLhzt(1);
        //添加到死薄
        userdie.setDieName(dsp.getFName())
                .setDieSex(dsp.getFSex())
                .setDieCountAge(dsp.getFCountAge())
                .setDieAge(dsp.getFCurrentAge())
                .setDieType(dsp.getFType())
                .setDieWhy(dsp.getFDieWhy())
                .setDieArea(dsp.getFArea())
                .setDieReincarnation(dsp.getFReincarnation())
                .setDieReinState(1)
                .setDieStatu(dsp.getFStatu())
                .setDieTime(dsp.getFDieTime())
                .setDieDesc(dsp.getFDesc())
                .setDieLassTime(Constants.CURRENT_TIME)
                .setDieCreatdate(Constants.CURRENT_TIME);
        //添加审判记录
        trial.setInfo(backVO.getInfo())
                .setTrialTime(Constants.CURRENT_TIME)
                .setTrialUserId(((User) session.getAttribute("UserInfo")).getUserId().intValue())
                .setUserId(dsp.getFFid())
                .setUserName(dsp.getFName())
                .setType(Constants.USERDIE_STATE_TRUE)
                .setTypes(1);
        //执行操作
        if (mengpoDao.addMengpo(mengpo) > 0) {
            if (dspDao.updateDspStatu(2, id) > 0) {
                if (trialDao.addTrial(trial) > 0) {
                    if (userdieDao.addUserdie(userdie) > 0) {
                        flag = true;
                    }
                }
            }
        }
        return flag ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }

    /**
     * @Description: 打入地狱
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object sendDiyu(DiyuVO readValue, Integer id, HttpSession session) {
        boolean flag = false;
        Dsp dsp = dspDao.getDspById(id);
        Userdie userdie = new Userdie();
        Xfxc xfxc = new Xfxc();
        Mingjie_Trial trial = new Mingjie_Trial();
        //添加到死薄
        userdie.setDieName(dsp.getFName())
                .setDieSex(dsp.getFSex())
                .setDieCountAge(dsp.getFCountAge())
                .setDieAge(dsp.getFCurrentAge())
                .setDieType(dsp.getFType())
                .setDieWhy(dsp.getFDieWhy())
                .setDieArea(dsp.getFArea())
                .setDieReincarnation(dsp.getFReincarnation())
                .setDieReinState(1)
                .setDieStatu(dsp.getFStatu())
                .setDieTime(dsp.getFDieTime())
                .setDieDesc(dsp.getFDesc())
                .setDieLassTime(Constants.CURRENT_TIME)
                .setDieCreatdate(Constants.CURRENT_TIME);
        //添加审判记录
        trial.setInfo(readValue.getInfo())
                .setTrialTime(Constants.CURRENT_TIME)
                .setTrialUserId(((User) session.getAttribute("UserInfo")).getUserId().intValue())
                .setUserId(dsp.getFFid())
                .setUserName(dsp.getFName())
                .setType(Constants.USERDIE_STATE_TRUE)
                .setTypes(2);
        //添加到刑法现场
        xfxc.setXUserId(readValue.getUserID())
                .setXUserName(readValue.getUserName())
                .setXSxsj(readValue.getYear())
                .setXDycj(readValue.getDycj())
                .setXCjsj(Constants.CURRENT_TIME)
                .setXOntime(Constants.CURRENT_DATE);
        //执行操作
        if (xfxcDao.addXfxc(xfxc) > 0) {
            if (dspDao.updateDspStatu(2, id) > 0) {
                if (trialDao.addTrial(trial) > 0) {
                    if (userdieDao.addUserdie(userdie) > 0) {
                        flag = true;
                    }
                }
            }
        }
        return flag ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }
}
