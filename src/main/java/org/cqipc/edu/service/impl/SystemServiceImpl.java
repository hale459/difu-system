package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.LoginLogDao;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 系统管理-服务实现
 * @ClassName: SystemServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
@Controller
@Transactional
public class SystemServiceImpl implements SystemService {

    @Autowired
    private LoginLogDao loginLogDao;


    /**
     * @Description: 查询登录日志记录
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getLoginLogList(Pagination pagination, String username, String date) {
        ResultTable resultTable = new ResultTable();
        return resultTable.setCode(StatusConstant.SUCCESS)
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setData(loginLogDao.getLoginLogList(pagination, username, date))
                .setCount(loginLogDao.getLoginLogListCout(username, date).size());
    }

    /**
     * @Description: 通过ID删除记录
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object deleteLoginLog(Integer id) {
        return loginLogDao.removeLoginLogById(id) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 批量删选
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchDelLoginLog(List<Integer> ids) {
        return loginLogDao.batchDelLoginLog(ids) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }
}
