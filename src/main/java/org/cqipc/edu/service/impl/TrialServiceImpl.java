package org.cqipc.edu.service.impl;

import org.cqipc.edu.dao.DspDao;
import org.cqipc.edu.service.TrialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: 审判记录-接口实现
 * @ClassName: TrialServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Service
@Transactional
public class TrialServiceImpl implements TrialService {

    @Autowired
    private DspDao dspDao;

    /**
     * @Description: 添加审判记录
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object addTrial(Integer id) {
        /*通过ID去查询待审判信息，并将信息添加审判记录中*/
        return null;
    }
}
