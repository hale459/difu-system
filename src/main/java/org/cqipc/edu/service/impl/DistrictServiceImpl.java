package org.cqipc.edu.service.impl;

import org.cqipc.edu.dao.DistrictDao;
import org.cqipc.edu.entity.District;
import org.cqipc.edu.service.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 地区联动-接口实现
 * @ClassName: DistrictServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Service
@Transactional
public class DistrictServiceImpl implements DistrictService {

    @Autowired
    private DistrictDao districtDao;


    /**
     * @Description: 查询所有地区信息
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(readOnly = true)
    public List<District> getAllList() {
        return districtDao.getAllList();
    }
}
