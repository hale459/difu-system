package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.Constants;
import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.MengpoDao;
import org.cqipc.edu.dao.TypeDao;
import org.cqipc.edu.dao.UserdieDao;
import org.cqipc.edu.dao.UserlifeDao;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Userlife;
import org.cqipc.edu.service.UserlifeService;
import org.cqipc.edu.vo.book.UserlifeAddVO;
import org.cqipc.edu.vo.book.UserlifeListVO;
import org.cqipc.edu.vo.book.UserlifeModifyVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 生薄用户-接口实现
 * @ClassName: UserlifeServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Service
@Transactional
public class UserlifeServiceImpl implements UserlifeService {

    @Autowired
    private UserlifeDao userlifeDao;

    @Autowired
    private TypeDao typeDao;

    @Autowired
    private MengpoDao mengpoDao;

    @Autowired
    private UserdieDao userdieDao;


    /**
     * @Description: 添加生薄用户
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object LivebookAdd(UserlifeAddVO bean, Integer mid) {
        boolean flag = false;
        //向生薄添加
        Userlife userlife = new Userlife();
        userlife.setLifeName(bean.getLifeName())
                .setLifeSex(bean.getLifeSex())
                .setLifeCountAge(bean.getLifeCountAge())
                .setLifeType(bean.getLifeType())
                .setLifeArea(bean.getLifeArea())
                .setLifeReincarnation(bean.getReincarnation())
                .setLifeCreatdate(Constants.CURRENT_TIME)
                .setLifeLassTime(Constants.CURRENT_TIME)
                .setLifeDieTime(Constants.CURRENT_TIME)
                .setLifeCurrentAge(Constants.USERLIFE_DEFAULT_AGE)
                .setLifeStatu(Constants.USERLIFE_DEFAULT_ASTATUS)
                .setLifeDieWhy(1)
                .setLifeDesc(userlife.getLifeDesc() == null || userlife.getLifeDesc().equals("") ?
                        Constants.USERLIFE_DEFAULT_DESC : bean.getLifeDesc());
        //执行操作
        if (userlifeDao.LivebookAdd(userlife) > 0) {
            if (mengpoDao.updateMengpoReinId(mid) > 0) {
                if (userdieDao.updateUserdieReinId(mengpoDao.getMengpoById(mid).getFFid()) > 0) {
                    flag = true;
                }
            }
        }
        return flag ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 分页/条件查询生薄用户
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getLivebookList(Pagination pagination, String username, Integer type) {
        ResultTable resultTable = new ResultTable();
        List<UserlifeListVO> list = new ArrayList<>();
        List<Userlife> livebookList = userlifeDao.getLivebookList(pagination, username, type);
        for (Userlife userlife : livebookList) {
            UserlifeListVO vo = new UserlifeListVO();
            vo.setId(userlife.getLifeId())
                    .setUsername(userlife.getLifeName())
                    .setSex(userlife.getLifeSex())
                    .setTotalAge(userlife.getLifeCurrentAge())
                    .setOverAge(userlife.getLifeCountAge() - userlife.getLifeCurrentAge())
                    .setType(typeDao.getTypeById(userlife.getLifeType()).getTypeName())
                    .setLass_updatetime(userlife.getLifeLassTime());
            list.add(vo);
        }
        resultTable.setCode(StatusConstant.SUCCESS)
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setCount(userlifeDao.getCountPage(username, type).size())
                .setData(list);
        return resultTable;
    }


    /**
     * @Description: 通过ID删除生薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object removeUserlifeById(Integer id) {
        return userlifeDao.removeUserlifeById(id) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 通过ID获取生薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Userlife getUserlifeById(Integer id) {
        return userlifeDao.getUserlifeById(id);
    }


    /**
     * @Description: 修改生薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object modifyUserlife(UserlifeModifyVO modifyVO) {
        Userlife oldData = userlifeDao.getUserlifeById(modifyVO.getId());
        Userlife newData = new Userlife();
        BeanUtils.copyProperties(oldData, newData);
        newData.setLifeName(modifyVO.getName())
                .setLifeSex(modifyVO.getSex())
                .setLifeCurrentAge(modifyVO.getCurrenAge())
                .setLifeCountAge(modifyVO.getOverAge() - modifyVO.getCurrenAge())
                .setLifeType(modifyVO.getType())
                .setLifeLassTime(Constants.CURRENT_TIME);
        return userlifeDao.modifyUserlife(newData) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 生薄用户批量删除
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchDelLive(List<Integer> item) {
        return userlifeDao.batchDelLive(item) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 获取所有生薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getUserlifeAll() {
        return userlifeDao.getUserlifeAll();
    }


    /**
     * @Description:
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getUserlifeByAreaId(Integer id) {
        return userlifeDao.getUserlifeByAreaId(id);
    }

}
