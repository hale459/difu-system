package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.UserDao;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.User;
import org.cqipc.edu.service.UserService;
import org.cqipc.edu.vo.user.UserListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 用户-接口实现
 * @ClassName: UserServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    /**
     * @Description: 分页/条件查询用户信息
     * @Author: Heyue
     * @Date: 2020/6/20
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getUserAll(Pagination pagination, Integer id, String username) {
        List<User> userAll = userDao.getUserAll(pagination, id, username);
        List<UserListVO> list = new ArrayList<>();
        ResultTable resultTable = new ResultTable();
        for (User user : userAll) {
            UserListVO vo = new UserListVO();
            BeanUtils.copyProperties(user, vo);
            vo.setId(user.getUserId())
                    .setSex(user.getSsex());
            list.add(vo);
        }
        resultTable.setCode(StatusConstant.SUCCESS)
                .setCount(userDao.getCountPage(id, username).size())
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setData(list);
        return resultTable;
    }


    /**
     * @Description: 用户登录查询(用户信息查询)
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public User UserLogin(String username, String password) {
//        return userDao.UserLogin(username, Md5.getMd5(password));
        return userDao.UserLogin(username, password);
    }


    /**
     * @Description: 批量删除--用户信息
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchDelUser(List<Integer> item) {
        return userDao.batchDelUser(item) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 根据用户ID删除用户信息
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object removeUserById(Integer id) {
        return userDao.removeUserById(id) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 根据用户ID获取用户信息
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getUserById(Integer id) {
        return userDao.getUserById(id);
    }


    /**
     * @Description: 根据用户名登录
     * @Author: Heyue
     * @Date: 2020/7/2
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public User getUserByUserName(String username) {
        return userDao.getUserByUserName(username);
    }
}
