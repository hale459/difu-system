package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.DeletedDao;
import org.cqipc.edu.dao.DiewhyDao;
import org.cqipc.edu.dao.UserdieDao;
import org.cqipc.edu.entity.Deleted;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Userdie;
import org.cqipc.edu.service.DeletedService;
import org.cqipc.edu.vo.user.DeletedListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 已删除用户-接口实现
 * @ClassName: DeletedServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-24
 **/
@Service
@Transactional
public class DeletedServiceImpl implements DeletedService {

    @Autowired
    private DeletedDao deletedDao;

    @Autowired
    private UserdieDao userdieDao;

    @Autowired
    private DiewhyDao diewhyDao;


    /**
     * @Description: 分页/条件查询所有已删除用户
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getDeletedList(Pagination pagination, Integer id, String username) {
        List<Deleted> deletedList = deletedDao.getDeletedList(pagination, id, username);
        List<DeletedListVO> list = new ArrayList<>();
        for (Deleted deleted : deletedList) {
            DeletedListVO vo = new DeletedListVO();
            BeanUtils.copyProperties(deleted, vo);
            vo.setDelWhy(diewhyDao.getDiewhyById(deleted.getDelWhy()).getName());
            list.add(vo);
        }
        ResultTable resultTable = new ResultTable();
        return resultTable.setCode(StatusConstant.SUCCESS)
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setCount(deletedDao.getDeletedAll().size())
                .setData(list);
    }


    /**
     * @Description: 批量删除已删除用户的信息(即永久删除无法轮回)
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchDelDeleted(List<Integer> item) {
        return deletedDao.batchDelDeleted(item) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 通过已删除用户的编号(ID)删除信息
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object removeDeletedById(Integer id) {
        return deletedDao.removeDeletedById(id) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }


    /**
     * @Description: 根据ID拿到已删除用户的信息
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getDeletedById(Integer id) {
        return deletedDao.getDeletedById(id);
    }


    /**
     * @Description: 退回死薄
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object backDiebook(Integer id) {
        Deleted deleted = deletedDao.getDeletedById(id);
        Userdie userdie = new Userdie();
        userdie.setDieId(deleted.getDelDieId())
                .setDieName(deleted.getDelName())
                .setDieSex(deleted.getDelSex())
                .setDieCountAge(deleted.getDelCountAge())
                .setDieAge(deleted.getDelAge())
                .setDieType(deleted.getDelType())
                .setDieWhy(deleted.getDelWhy())
                .setDieArea(deleted.getDelArea())
                .setDieReincarnation(deleted.getDelReincarnation())
                .setDieDesc(deleted.getDelDesc())
                .setDieReinState(deleted.getDelReinState())
                .setDieStatu(deleted.getDelStatu())
                .setDieTime(deleted.getDelTime())
                .setDieCreatdate(deleted.getDelCreatdate())
                .setDieLassTime(deleted.getDelLassTime());
        deletedDao.removeDeletedById(id);
        return userdieDao.insertUserDieBack(userdie) > 0 ? StatusConstant.MSG_SUCCESS : StatusConstant.MSG_ERROR;
    }
}
