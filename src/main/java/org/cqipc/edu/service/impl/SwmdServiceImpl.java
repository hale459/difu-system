package org.cqipc.edu.service.impl;

import org.cqipc.edu.common.ResultTable;
import org.cqipc.edu.common.StatusConstant;
import org.cqipc.edu.dao.DistrictDao;
import org.cqipc.edu.dao.SwmdDao;
import org.cqipc.edu.dao.TypeDao;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Swmd;
import org.cqipc.edu.service.SwmdService;
import org.cqipc.edu.vo.plague.DetailsVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 死亡名单-接口实现
 * @ClassName: SwmdServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-26
 **/
@Service
@Transactional
public class SwmdServiceImpl implements SwmdService {

    @Autowired
    private SwmdDao swmdDao;

    @Autowired
    private TypeDao typeDao;

    @Autowired
    private DistrictDao districtDao;

    /**
     * @Description: 批量添加死亡名单
     * @Author: Heyue
     * @Date: 2020/6/26
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchAddSwmd(List<Swmd> swmd) {
        return swmdDao.batchAddSwmd(swmd);
    }


    /**
     * @Description: 分页查询死亡信息
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getSwmdList(Pagination pagination, Integer id) {
        ResultTable resultTable = new ResultTable();
        List<DetailsVO> list = new ArrayList<>();
        for (Swmd swmd : swmdDao.getSwmdPage(pagination, id)) {
            DetailsVO vo = new DetailsVO();
            BeanUtils.copyProperties(swmd, vo);
            vo.setDtype(typeDao.getTypeById(swmd.getDtype()).getTypeName())
                    .setDarea(districtDao.getDistrictById(swmd.getDarea()).getDistrictName());
            list.add(vo);
        }
        return resultTable.setData(list)
                .setCode(0)
                .setMsg(StatusConstant.MSG_DEFAULT)
                .setCount(swmdDao.getSwmdPageCount(id).size());
    }
}
