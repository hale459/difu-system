package org.cqipc.edu.service.impl;

import org.cqipc.edu.dao.DeptDao;
import org.cqipc.edu.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: 部门-接口实现
 * @ClassName: DeptServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
@Controller
@Transactional
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;


    /**
     * @Description: 获取所有部门信息
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Object getDeptList() {
        return deptDao.getDeptList();
    }
}
