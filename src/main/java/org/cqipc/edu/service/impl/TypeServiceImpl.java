package org.cqipc.edu.service.impl;

import org.cqipc.edu.dao.TypeDao;
import org.cqipc.edu.entity.Type;
import org.cqipc.edu.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 类别-接口实现
 * @ClassName: TypeServiceImpl
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Service
@Transactional
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeDao typeDao;


    /**
     * @Description: 获取所有类别信息
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Type> getTypeAll() {
        return typeDao.getTypeAll();
    }


    /**
     * @Description: 通过类别ID获取类别信息
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Type getTypeById(Integer id) {
        return typeDao.getTypeById(id);
    }

}
