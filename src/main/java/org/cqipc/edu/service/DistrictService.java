package org.cqipc.edu.service;

import org.cqipc.edu.entity.District;

import java.util.List;

/**
 * @Description: 地区联动-服务接口
 * @ClassName: DistrictService
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface DistrictService {

    //获取所有地区
    List<District> getAllList();

}
