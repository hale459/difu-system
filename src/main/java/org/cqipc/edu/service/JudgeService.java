package org.cqipc.edu.service;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.vo.judge.BackVO;
import org.cqipc.edu.vo.trial.DiyuVO;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Description: 审判-服务接口
 * @ClassName: JudgeService
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
public interface JudgeService {

    //获取【待审判】表格数据
    Object getDspList(Pagination pagination, String username, String date);

    //获取【已审判】表格数据
    Object getDspdList(Pagination pagination, String username, String date);

    //通过ID删除【待审判记录】
    Object delDspById(Integer id);

    //批量删除【待审判记录】
    Object batchDsp(List<Integer> ids);

    //通过ID获取审判记录
    Object getInfoById(Integer id);

    //退单操作
    Object backOrder(BackVO backVO, Integer id, HttpSession session);

    //安排投胎
    Object Reincarnation(BackVO backVO, Integer id, HttpSession session);

    //打入地狱
    Object sendDiyu(DiyuVO readValue, Integer id, HttpSession session);
}
