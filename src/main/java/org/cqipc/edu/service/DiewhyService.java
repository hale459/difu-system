package org.cqipc.edu.service;

/**
 * @Description: 死亡原因-服务接口
 * @ClassName: DiewhyService
 * @Author: Heyue
 * @DateTime: 2020-06-21
 **/
public interface DiewhyService {

    //获取所有死亡原因信息
    Object getDiewhyAll();

    //根据ID获取死亡原因
    Object getDiewhyById(Integer id);
}
