package org.cqipc.edu.service;

import org.cqipc.edu.entity.Type;

import java.util.List;

/**
 * @Description: 类别-服务接口
 * @ClassName: TypeService
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface TypeService {

    //获取所有类别
    List<Type> getTypeAll();

    //根据类型ID拿到对应类型
    Type getTypeById(Integer id);

}
