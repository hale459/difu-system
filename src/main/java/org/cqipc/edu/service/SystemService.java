package org.cqipc.edu.service;

import org.cqipc.edu.entity.Pagination;

import java.util.List;

/**
 * @Description: 系统管理-服务接口
 * @ClassName: SystemService
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
public interface SystemService {

    //查询登录日志
    Object getLoginLogList(Pagination pagination, String username, String date);

    //通过ID删除记录
    Object deleteLoginLog(Integer id);

    //批量删除
    Object batchDelLoginLog(List<Integer> ids);
}
