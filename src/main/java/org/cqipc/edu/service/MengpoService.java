package org.cqipc.edu.service;

import org.cqipc.edu.entity.Pagination;

/**
 * @Description: 孟婆管理-服务接口
 * @ClassName: MengpoService
 * @Author: Heyue
 * @DateTime: 2020-06-29
 **/
public interface MengpoService {

    //分页/条件获取所有孟婆的数据
    Object getMengpoAll(Pagination pagination, Integer diewhy, String username);

}
