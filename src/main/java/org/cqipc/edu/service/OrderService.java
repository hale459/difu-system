package org.cqipc.edu.service;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Pagination;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Description: 订单管理-数据接口
 * @ClassName: OrderService
 * @Author: Heyue
 * @DateTime: 2020-06-27
 **/
public interface OrderService {

    //获取死亡后未被分匹配的人
    Object getUnassigned(@Param("page") Pagination pagination, @Param("username") String username, @Param("date") String date,
                         @Param("statu") Integer statu);

    //批量分配
    Object batchUnassigned(List<Integer> id, HttpSession session);

    //通过ID分配
    Object deleteUnassignedById(Integer id, HttpSession session);

    //分页/条件查询未分配的人
    Object getFpeiPage(Pagination pagination, Integer statu, String username);

    //批量勾魂
    Object batchGouhun(List<Integer> ids);

    //勾魂操作
    Object removeFpeiById(Integer id);

    //通过ID删除
    Object delFpeiById(Integer id);

}
