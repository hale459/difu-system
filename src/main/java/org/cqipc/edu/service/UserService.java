package org.cqipc.edu.service;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.User;

import java.util.List;

/**
 * @Description: 用户-服务接口
 * @ClassName: UserService
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface UserService {

    //分页/条件查询用户信息
    Object getUserAll(Pagination pagination, Integer id, String username);

    //用户登录1
    User UserLogin(String username, String password);

    //批量删除--用户信息
    Object batchDelUser(List<Integer> item);

    //根据用户ID删除用户信息
    Object removeUserById(Integer id);

    //根据用户ID获取用户信息
    Object getUserById(Integer id);

    //根据用户名登录
    User getUserByUserName(String username);

}
