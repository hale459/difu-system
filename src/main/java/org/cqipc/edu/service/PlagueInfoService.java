package org.cqipc.edu.service;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Plague_Info;

import java.util.List;

/**
 * @Description: 瘟疫情况-服务接口
 * @ClassName: PlagueInfoService
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
public interface PlagueInfoService {

    //添加瘟疫情况信息
    Object addPlagueInfo(Plague_Info plagueInfo);

    //分页/条件查询瘟疫情况
    Object getPlagueInfoList(Pagination pagination, String username, Integer plague, Integer area);

    //批量删除瘟疫情况表
    Object batchDelPlagueInfo(List<Integer> id);

    //通过ID删除瘟疫信息
    Object removePlaugeInfoByid(Integer id);

    //通过ID查询瘟疫信息
    Object getPlaugeInfoById(Integer id);


}
