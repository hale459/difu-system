package org.cqipc.edu.common;

/**
 * @Description: 状态常量
 * @ClassName: StatusConstant
 * @Author: Heyue
 * @Date: 2020/6/20
 */
public class StatusConstant {

    public static final Integer SUCCESS = 0;
    public static final Integer ERROR = -1;
    public static final String MSG_ERROR = "error";
    public static final String MSG_SUCCESS = "success";
    public static final String MSG_DEFAULT = "";

}
