package org.cqipc.edu.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 欢迎页数据展示
 * @ClassName: WelcomeData
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class WelcomeData implements Serializable {

    private Integer lifeCount;
    private Integer dieCount;
    private Integer dfpCount;
    private Integer dspCount;

}
