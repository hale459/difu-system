package org.cqipc.edu.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 数据表格返回格式
 * @ClassName: Result
 * @Author: Heyue
 * @Date: 2020/6/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ResultTable<T> implements Serializable {

    private Integer code;
    private String msg;
    private Integer count;
    private List<T> data;

}
