package org.cqipc.edu.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description: 常用常量
 * @ClassName: Constants
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public class Constants {

    public static final Integer USERLIFE_DEFAULT_AGE = 0;//默认当前年龄
    public static final Integer USERLIFE_DEFAULT_ASTATUS = 1;//默认死亡状态
    public static final String USERLIFE_DEFAULT_DESC = "暂无";//默认描述信息
    public static final Integer USERDIE_STATE_FALSE = 1;//死亡状态(未死亡)
    public static final Integer USERDIE_STATE_TRUE = 2;//死亡状态(已死亡)
    public static final String CURRENT_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());//获取当前时间
    public static final String CURRENT_DATE = new SimpleDateFormat("yyyy-MM-dd").format(new Date());//获取当前日期


}
