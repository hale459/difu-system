package org.cqipc.edu.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 死亡类型返回JSON格式
 * @ClassName: TypeResult
 * @Author: Heyue
 * @DateTime: 2020-06-23
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TypeResult implements Serializable {

    private Integer dieType;
    private Integer dieWhy;

}
