package org.cqipc.edu.controller;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.PlagueInfoService;
import org.cqipc.edu.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 瘟疫情况控制器
 * @ClassName: PlagueInfoController
 * @Author: Heyue
 * @DateTime: 2020-06-26
 **/
@Controller
public class PlagueInfoController {

    @Autowired
    private PlagueInfoService plagueInfoService;


    /**
     * @Description: 分页/条件查询瘟疫情况
     * @Author: Heyue
     * @Date: 2020/6/26
     */
    @GetMapping("/getPlagueInfoList")
    @ResponseBody
    public Object getPlagueInfoList(Integer page, Integer limit, String username, Integer plague, Integer area) {
        return plagueInfoService.getPlagueInfoList(new Pagination(page, limit), username, plague, area);
    }


    /**
     * @Description: 批量删除瘟疫情况表
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @DeleteMapping("/batchDelPlagueInfo")
    @ResponseBody
    public Object batchDelPlagueInfo(@RequestParam("ids") String ids) {
        return plagueInfoService.batchDelPlagueInfo(CommonUtil.StringToIntegerlIST(ids, "\\,"));
    }


    /**
     * @Description: 通过ID删除瘟疫情况信息
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @DeleteMapping("/deletePlaugeInfoByid/{tableId}")
    @ResponseBody
    public Object removePlaugeInfoByid(@PathVariable("tableId") Integer tableId) {
        return plagueInfoService.removePlaugeInfoByid(tableId);
    }


    /**
     * @Description: 通过ID删除瘟疫情况信息
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @GetMapping("/getPlaugeInfoById/{tableId}")
    @ResponseBody
    public Object getPlaugeInfoById(@PathVariable("tableId") Integer tableId) {
        return plagueInfoService.getPlaugeInfoById(tableId);
    }


}
