package org.cqipc.edu.controller;

import org.cqipc.edu.service.PlagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description: 瘟疫控制器
 * @ClassName: PlagueController
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
@Controller
public class PlagueController {

    @Autowired
    private PlagueService plagueService;


    /**
     * @Description: 发布瘟疫处理
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @PutMapping("/sendPlague/{plague}/{area}/{userlife}/{description}")
    @ResponseBody
    public Object sendPlague(@PathVariable("plague") Integer plague, @PathVariable("area") Integer area,
                             @PathVariable("userlife") Integer userlife, @PathVariable("description") String description) {
        return plagueService.sendPlague(plague, area, userlife, description);
    }

}
