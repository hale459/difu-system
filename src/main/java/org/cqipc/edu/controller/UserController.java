package org.cqipc.edu.controller;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.UserService;
import org.cqipc.edu.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 用户控制器
 * @ClassName: UserController
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Controller
public class UserController {

    @Autowired
    private UserService userService;


    /**
     * @Description: 分页/条件查询用户信息
     * @Author: Heyue
     * @Date: 2020/6/20
     */
    @GetMapping("/getUserList")
    @ResponseBody
    public Object getUserAll(Integer page, Integer limit, Integer id, String username) {
        return userService.getUserAll(new Pagination(page, limit), id, username);
    }


    /**
     * @Description: 根据用户ID删除用户信息
     * @Author: Heyue
     * @Date: 2020/6/21
     */
    @DeleteMapping("/removeUserById/{tableId}")
    @ResponseBody
    public Object removeUserById(@PathVariable("tableId") Integer tableId) {
        return userService.removeUserById(tableId);
    }


    /**
     * @Description: 批量删除--用户信息
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @DeleteMapping("/batchDelUser")
    @ResponseBody
    public Object batchDelUser(@RequestParam("ids") String ids) {
        return userService.batchDelUser(CommonUtil.StringToIntegerlIST(ids, "\\,"));
    }


    /**
     * @Description: 根据用户ID获取用户信息
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @GetMapping("/getUserById/{id}")
    @ResponseBody
    public Object getUserById(@PathVariable("id") Integer id) {
        return userService.getUserById(id);
    }
}
