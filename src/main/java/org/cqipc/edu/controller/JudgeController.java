package org.cqipc.edu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.JudgeService;
import org.cqipc.edu.utils.CommonUtil;
import org.cqipc.edu.vo.judge.BackVO;
import org.cqipc.edu.vo.trial.DiyuVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * @Description: 审判控制器
 * @ClassName: JudgeController
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Controller
public class JudgeController {

    @Autowired
    private JudgeService judgeService;

    /**
     * @Description: 获取【待审判】表格数据
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @GetMapping("/getDspList")
    @ResponseBody
    public Object getDspList(Integer page, Integer limit, String username, String date) {
        return judgeService.getDspList(new Pagination(page, limit), username, date);
    }

    /**
     * @Description: 获取【已审判】表格数据
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @GetMapping("/getDspdList")
    @ResponseBody
    public Object getDspdList(Integer page, Integer limit, String username, String date) {
        return judgeService.getDspdList(new Pagination(page, limit), username, date);
    }


    /**
     * @Description: 通过ID删除【待审判】记录
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @DeleteMapping("/delDspById/{id}")
    @ResponseBody
    public Object delDspById(@PathVariable("id") Integer id) {
        return judgeService.delDspById(id);
    }


    /**
     * @Description: 批量勾魂
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @DeleteMapping("/batchDsp")
    @ResponseBody
    public Object batchGouhun(@RequestParam("ids") String ids) {
        return judgeService.batchDsp(CommonUtil.StringToIntegerlIST(ids, "\\,"));
    }


    /**
     * @Description: 退单操作
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @PostMapping("/backOrder/{id}")
    @ResponseBody
    public Object backOrder(HttpSession session, @RequestParam("info") String info, @PathVariable("id") Integer id) throws JsonProcessingException {
        BackVO backVO = new ObjectMapper().readValue(info, BackVO.class);
        return judgeService.backOrder(backVO, id, session);
    }

    /**
     * @Description: 安排投胎
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @PostMapping("/reincarnation/{id}")
    @ResponseBody
    public Object reincarnation(HttpSession session, @RequestParam("info") String info, @PathVariable("id") Integer id) throws JsonProcessingException {
        BackVO backVO = new ObjectMapper().readValue(info, BackVO.class);
        return judgeService.Reincarnation(backVO, id, session);
    }


    /**
     * @Description: 打入地狱
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @PostMapping("/sendDiyu/{id}")
    @ResponseBody
    public Object sendDiyu(@RequestParam("dyInfo") String info, @PathVariable("id") Integer id, HttpSession session) throws JsonProcessingException {
        return judgeService.sendDiyu(new ObjectMapper().readValue(info, DiyuVO.class), id, session);
    }


    /**
     * @Description: 通过ID获取审判记录
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @GetMapping("/getInfo/{id}")
    @ResponseBody
    public Object getInfo(@PathVariable("id") Integer id) {
        return judgeService.getInfoById(id);
    }

}
