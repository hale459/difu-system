package org.cqipc.edu.controller;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.MengpoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description: 孟婆管理控制器
 * @ClassName: MengpoController
 * @Author: Heyue
 * @DateTime: 2020-06-29
 **/
@Controller
public class MengpoController {

    @Autowired
    private MengpoService mengpoService;


    /**
     * @Description: 分页/条件获取孟婆信息【表格】
     * @Author: Heyue
     * @Date: 2020/6/29
     */
    @GetMapping("/getMengpoList")
    @ResponseBody
    public Object getMengpoList(Integer page, Integer limit, Integer diewhy, String username) {
        return mengpoService.getMengpoAll(new Pagination(page, limit), diewhy, username);
    }

}
