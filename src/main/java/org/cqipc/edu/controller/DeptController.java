package org.cqipc.edu.controller;

import org.cqipc.edu.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description: 部门控制器
 * @ClassName: DeptController
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
@Controller
public class DeptController {

    @Autowired
    private DeptService deptService;

    /**
     * @Description: 获取所有部门信息(下拉列表)
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @GetMapping("/getDeptList")
    @ResponseBody
    public Object getDeptList() {
        return deptService.getDeptList();
    }

}
