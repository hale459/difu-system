package org.cqipc.edu.controller;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.SwmdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description: 死亡名单控制器
 * @ClassName: SwmdController
 * @Author: Heyue
 * @DateTime: 2020-06-27
 **/
@Controller
public class SwmdController {

    @Autowired
    private SwmdService swmdService;


    /**
     * @Description: 分页获取死亡名单信息
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @GetMapping("/getSwmdList/{id}")
    @ResponseBody
    public Object getSwmdList(Integer page, Integer limit, @PathVariable("id") Integer id) {
        return swmdService.getSwmdList(new Pagination(page, limit), id);
    }

}
