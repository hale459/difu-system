package org.cqipc.edu.controller;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.DeletedService;
import org.cqipc.edu.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 已删除用户控制器
 * @ClassName: DeletedController
 * @Author: Heyue
 * @DateTime: 2020-06-24
 **/
@Controller
public class DeletedController {

    @Autowired
    private DeletedService deletedService;


    /**
     * @Description: 分页/条件查询所有已删除用户信息
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @GetMapping("/getDeleteList")
    @ResponseBody
    public Object getDeleteList(Integer page, Integer limit, Integer id, String username) {
        return deletedService.getDeletedList(new Pagination(page, limit), id, username);
    }


    /**
     * @Description: 批量删除已删除的用户信息
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @DeleteMapping("/batchDelDeleted")
    @ResponseBody
    public Object batchDelDeleted(@RequestParam("ids") String ids) {
        return deletedService.batchDelDeleted(CommonUtil.StringToIntegerlIST(ids, "\\,"));
    }


    /**
     * @Description: 通过表格中的ID删除已删除用户信息
     * @Author: Heyue
     * @Date: 2020/6/21
     */
    @DeleteMapping("/deleteByid/{tableId}")
    @ResponseBody
    public Object removeDeletedById(@PathVariable("tableId") Integer tableId) {
        return deletedService.removeDeletedById(tableId);
    }


    /**
     * @Description: 通过表格ID拿到已删除用户信息
     * @Author: Heyue
     * @Date: 2020/6/21
     */
    @GetMapping("/getDeletedById/{tableId}")
    @ResponseBody
    public Object getDeletedById(@PathVariable("tableId") Integer tableId) {
        return deletedService.getDeletedById(tableId);
    }


    /**
     * @Description: 退回死薄
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @PostMapping("/backDiebook/{id}")
    @ResponseBody
    public Object backDiebook(@PathVariable("id") Integer id) {
        return deletedService.backDiebook(id);
    }


}
