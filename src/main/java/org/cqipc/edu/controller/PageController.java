package org.cqipc.edu.controller;

import org.apache.shiro.authz.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description: 页面控制器
 * @ClassName: PageController
 * @Author: Heyue
 * @DateTime: 2020-07-02
 **/
@Controller
public class PageController {

    /*
      @RequiresAuthentication//必须要认证后才能访问
      @RequiresUser//拥有记住我时可访问
      @RequiresGuest//游客访问(无需认证)
      @RequiresRoles(value = {"ADMIN", "USER"}, logical = Logical.OR)//表示需要角色ADMIN或USER才能访问
      @RequiresPermissions(value = {"perm:add","perm:update"},logical = Logical.AND)//表示只有两个权限同时存在时才能访问
     */


    /*初始化*/
    @RequiresUser
    @RequiresRoles(value = {"ADMIN", "USER", "GUEST"}, logical = Logical.OR)
    @RequestMapping({"/", "/index"})
    public String index() {
        return "index";
    }

    @RequiresGuest
    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequiresRoles(value = {"ADMIN", "USER", "GUEST"}, logical = Logical.OR)
    @RequestMapping("/welcome")
    public String welcome() {
        return "view/welcome";
    }


    /*生死薄管理*/
    @RequestMapping("/livebook-add")
    public String livebookAdd() {
        return "view/book/operating/livebook-add";
    }

    @GetMapping("/livebook-edit")
    public String livebookEdit() {
        return "view/book/operating/livebook-edit";
    }

    @GetMapping("/diebook-edit")
    public String diebookEdit() {
        return "view/book/operating/diebook-edit";
    }

    @RequiresRoles(value = {"ADMIN", "USER"}, logical = Logical.OR)
    @RequestMapping("/livebook")
    public String livebook() {
        return "view/book/livebook";
    }

    @RequiresRoles(value = {"ADMIN", "USER"}, logical = Logical.OR)
    @RequestMapping("/diebook")
    public String diebook() {
        return "view/book/diebook";
    }

    @RequestMapping("/diebook-add")
    public String diebookAdd() {
        return "view/book/operating/diebook-add";
    }


    /*订单管理*/
    @RequiresRoles(value = {"USER", "ADMIN"}, logical = Logical.OR)
    @RequestMapping("/order-unassigned")
    public String unassigned() {
        return "view/order/unassigned";
    }

    @RequestMapping("/unassigned-look")
    public String unassignedLook() {
        return "view/order/operating/unassigned-look";
    }

    @RequiresRoles(value = {"USER", "ADMIN"}, logical = Logical.OR)
    @RequestMapping("/allocated")
    public String allocated() {
        return "view/order/allocated";
    }


    /*用户管理*/
    @RequestMapping("/user-password")
    public String userPassword() {
        return "view/user/user-password";
    }

    @RequestMapping("/user-setting")
    public String userSetting() {
        return "view/user/user-setting";
    }

    @RequiresRoles("ADMIN")
    @RequestMapping("/user-list")
    public String userList() {
        return "view/user/user-list";
    }

    @RequestMapping("/user-edit")
    public String userEdit() {
        return "view/user/user-edit";
    }

    @RequestMapping("/user-look")
    public String userLook() {
        return "view/user/user-look";
    }

    @RequiresRoles("ADMIN")
    @RequestMapping("/user-deleted")
    public String userDeleted() {
        return "view/user/user-deleted";
    }


    /*瘟疫管理*/
    @RequestMapping("/release")
    public String release() {
        return "view/plague/release";
    }

    @RequiresRoles(value = {"ADMIN", "USER"}, logical = Logical.OR)
    @RequestMapping("/record")
    public String record() {
        return "view/plague/record";
    }

    @RequestMapping("/details")
    public String details() {
        return "view/plague/operating/details";
    }


    /*判官管理*/
    @RequiresRoles("ADMIN")
    @RequestMapping("/untrial")
    public String untrial() {
        return "view/judge/untrial";
    }

    @RequiresRoles("ADMIN")
    @RequestMapping("/tried")
    public String tried() {
        return "view/judge/tried";
    }

    @RequestMapping("/trialdetails")
    public String trialdetails() {
        return "view/judge/operating/trialdetails";
    }

    @RequestMapping("/trialLog")
    public String trialLog() {
        return "view/judge/operating/trialLog";
    }

    @RequestMapping("/diyu")
    public String diyu() {
        return "view/judge/operating/diyu";
    }


    /*孟婆管理*/
    @RequiresRoles("ADMIN")
    @RequestMapping("/mengpo")
    public String mengpo() {
        return "view/mengpo/mengpo";
    }


    /*地狱管理*/
    @RequiresRoles("ADMIN")
    @RequestMapping("/penalty")
    public String penalty() {
        return "view/penalty/penalty";
    }

    @RequiresRoles(value = {"ADMIN", "USER", "GUEST"}, logical = Logical.OR)
    @RequestMapping("/penaltyLog")
    public String penaltyLog() {
        return "view/penalty/penaltyLog";
    }


    /*系统管理*/
    @RequestMapping("/admin")
    public String admin() {
        return "view/system/admin/admin";
    }

    @RequiresRoles("ADMIN")
    @RequestMapping("/log")
    public String log() {
        return "view/system/log/log";
    }

    @RequestMapping("/role")
    public String role() {
        return "view/system/role/role";
    }


    /*其他页面管理*/
    @RequiresGuest
    @RequestMapping("/403")
    public String unauthorized() {
        return "view/other/403";
    }

    @RequiresGuest
    @RequestMapping("/404")
    public String notFound() {
        return "view/other/404";
    }

    @RequiresGuest
    @RequestMapping("/500")
    public String serverError() {
        return "view/other/500";
    }
}
