package org.cqipc.edu.controller;

import org.cqipc.edu.common.Constants;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.OrderService;
import org.cqipc.edu.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * @Description: 勾魂/分配控制器
 * @ClassName: OrderController
 * @Author: Heyue
 * @DateTime: 2020-06-21
 **/
@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * @Description: 获取死后未分配(未勾魂)的用户
     * @Author: Heyue
     * @Date: 2020/6/21
     */
    @GetMapping("/getUnassigned")
    @ResponseBody
    public Object getUnassigned(Integer page, Integer limit, String username, String date) {
        return orderService.getUnassigned(new Pagination(page, limit), username, date, Constants.USERDIE_STATE_TRUE);
    }


    /**
     * @Description: 批量分配
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @DeleteMapping("/batchUnassigned")
    @ResponseBody
    public Object batchUnassigned(@RequestParam("ids") String ids, HttpSession session) {
        return orderService.batchUnassigned(CommonUtil.StringToIntegerlIST(ids, "\\,"), session);
    }


    /**
     * @Description: 通过ID分配
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @DeleteMapping("/deleteUnassignedById/{tableId}")
    @ResponseBody
    public Object deleteUnassignedById(@PathVariable("tableId") Integer tableId, HttpSession session) {
        return orderService.deleteUnassignedById(tableId, session);
    }


    /**
     * @Description: 分页/条件获取已分配的人-表格
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @GetMapping("/getFpeiList")
    @ResponseBody
    public Object getFpeiList(Integer page, Integer limit, Integer statu, String username) {
        return orderService.getFpeiPage(new Pagination(page, limit), statu, username);
    }


    /**
     * @Description: 批量勾魂
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @DeleteMapping("/batchGouhun")
    @ResponseBody
    public Object batchGouhun(@RequestParam("ids") String ids) {
        return orderService.batchGouhun(CommonUtil.StringToIntegerlIST(ids, "\\,"));
    }

    /**
     * @Description: 透过ID勾魂
     * @Author: Heyue
     * @Date: 2020/6/27
     */
    @DeleteMapping("/removeFpeiById/{id}")
    @ResponseBody
    public Object removeFpeiById(@PathVariable("id") Integer id) {
        return orderService.removeFpeiById(id);
    }


    /**
     * @Description: 通过ID删除已分配的记录
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @DeleteMapping("/delFpeiById/{id}")
    @ResponseBody
    public Object delFpeiById(@PathVariable("id") Integer id) {
        return orderService.delFpeiById(id);
    }
}