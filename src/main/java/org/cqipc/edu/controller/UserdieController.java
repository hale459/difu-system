package org.cqipc.edu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.DiewhyService;
import org.cqipc.edu.service.UserdieService;
import org.cqipc.edu.utils.CommonUtil;
import org.cqipc.edu.vo.book.UserdieModifyVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 死薄控制器
 * @ClassName: UserdieController
 * @Author: Heyue
 * @DateTime: 2020-06-21
 **/
@Controller
public class UserdieController {

    @Autowired
    private UserdieService userdieService;

    @Autowired
    private DiewhyService diewhyService;


    /**
     * @Description: 分页/条件查询死薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/21
     */
    @GetMapping("/getDiebookList")
    @ResponseBody
    public Object getDiebookList(Integer page, Integer limit, String username, Integer type, Integer dieWhy) {

        return userdieService.getDiebookList(new Pagination(1, 8), username, type, dieWhy);
    }


    /**
     * @Description: 通过死用户ID查询对应信息(类型 / 死亡类型编号)
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @GetMapping("/getUserDieById/{id}")
    @ResponseBody
    public Object getUserlifeById(@PathVariable("id") Integer id) {
        return userdieService.getUserdieById(id);
    }


    /**
     * @Description: 通过死用户ID查询对应信息(所有死亡原因)
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @GetMapping("/getDieWhyList")
    @ResponseBody
    public Object getDieWhy() {
        return diewhyService.getDiewhyAll();
    }


    /**
     * @Description: 修改死薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/23
     */
    @PutMapping("/diebookModify")
    @ResponseBody
    public Object modifyUserdie(@RequestParam("updateInfo") String updateInfo) throws JsonProcessingException {
        return userdieService.modifyUserdie(new ObjectMapper().readValue(updateInfo, UserdieModifyVO.class));
    }


    /**
     * @Description: 通过表格中的ID删除死薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/21
     */
    @DeleteMapping("/deleteDiebookByid/{tableId}")
    @ResponseBody
    public Object removeUserdieById(@PathVariable("tableId") Integer tableId) {
        return userdieService.removeUserdieById(tableId);
    }


    /**
     * @Description: 批量删除--死薄用户
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @DeleteMapping("/batchDelDie")
    @ResponseBody
    public Object batchDelDie(@RequestParam("ids") String ids) {
        return userdieService.batchDelDie(CommonUtil.StringToIntegerlIST(ids, "\\,"));
    }
}
