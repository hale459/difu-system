package org.cqipc.edu.controller;

import org.cqipc.edu.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 数据控制器
 * @ClassName: DataController
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@RestController
public class DataController {

    @Autowired
    private TypeService typeService;

    @Autowired
    private DistrictService districtService;

    @Autowired
    private DiewhyService diewhyService;

    @Autowired
    private UserlifeService userlifeService;

    @Autowired
    private PlagueService plagueService;

    @Autowired
    private EighteenService eighteenService;

    /**
     * @Description: 获取到所有的类别并渲染到前台下拉列表框
     * @ClassName: DataController
     * @Author: Heyue
     * @Date: 2020/6/20
     */
    @GetMapping("/getTypeList")
    public Object getTypeList() {
        return typeService.getTypeAll();
    }

    /**
     * @Description: 获取到所有的地区并渲染到前台下拉列表框
     * @Author: Heyue
     * @Date: 2020/6/20
     */
    @GetMapping("/getAreaList")
    public Object getAreaList() {
        return districtService.getAllList();
    }


    /**
     * @Description: 根据死亡原因ID拿到死亡原因信息
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @GetMapping("/getDiewhyById/{id}")
    public Object getDiewhyById(@PathVariable("id") Integer id) {
        return diewhyService.getDiewhyById(id);
    }


    /**
     * @Description: 获取所有死亡原因到下拉列表框
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    @GetMapping("/getDiewhyList")
    public Object getDiewhyList() {
        return diewhyService.getDiewhyAll();
    }


    /**
     * @Description: 获取所有生薄用户信息-下拉列表
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @GetMapping("/getLifeuser")
    public Object getLifeuser() {
        return userlifeService.getUserlifeAll();
    }


    /**
     * @Description: 根据地区ID查询用户信息-下拉列表
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @GetMapping("/getUserlifeByAreaId/{id}")
    public Object getUserlifeByAreaId(@PathVariable("id") Integer id) {
        return userlifeService.getUserlifeByAreaId(id);
    }

    /**
     * @Description: 根据瘟疫ID查询瘟疫信息-下拉列表
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @GetMapping("/getUserlifeByPlaugeId/{id}")
    public Object getUserlifeByPlaugeId(@PathVariable("id") Integer id) {
        return plagueService.getPlagueById(id);
    }


    /**
     * @Description: 查询所有瘟疫信息-下拉列表
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @GetMapping("/getPlagueList")
    public Object getPlagueList() {
        return plagueService.getPlagueAll();
    }


    /**
     * @Description: 查询地狱层级信息-下拉列表
     * @Author: Heyue
     * @Date: 2020/6/25
     */
    @GetMapping("/getEighteenLevel")
    public Object getEighteenLevel() {
        return eighteenService.getEighteenLevel();
    }


}
