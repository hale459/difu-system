package org.cqipc.edu.controller;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.SystemService;
import org.cqipc.edu.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 系统管理控制器
 * @ClassName: SystemController
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
@Controller
public class SystemController {

    @Autowired
    private SystemService systemService;

    /**
     * @Description: 获取登录日志列表
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @GetMapping("/getLoginLogList")
    @ResponseBody
    public Object getLoginLogList(Integer page, Integer limit, String username, String date) {
        return systemService.getLoginLogList(new Pagination(page, limit), username, date);
    }

    /**
     * @Description: 通过ID删除
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @DeleteMapping("/deleteLoginLog/{id}")
    @ResponseBody
    public Object deleteLoginLog(@PathVariable("id") Integer id) {
        return systemService.deleteLoginLog(id);
    }


    @DeleteMapping("/batchDelLoginLog")
    @ResponseBody
    public Object batchDelLoginLog(@RequestParam("ids") String ids) {
        return systemService.batchDelLoginLog(CommonUtil.StringToIntegerlIST(ids, "\\,"));
    }


}
