package org.cqipc.edu.controller;

import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.XfService;
import org.cqipc.edu.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 刑罚控制器
 * @ClassName: XfController
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
@Controller
public class XfController {

    @Autowired
    private XfService xfService;

    /**
     * @Description: 获取所有刑罚现场
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @GetMapping("/getXfxcList")
    @ResponseBody
    public Object getXfxcList(Integer page, Integer limit, String username, Integer level) {
        return xfService.getXfxcList(new Pagination(page, limit), username, level);
    }


    /**
     * @Description: 提前释放
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @DeleteMapping("/updateXf/{id}")
    @ResponseBody
    public Object updateXf(@PathVariable("id") Integer id) {
        return xfService.updateXf(id);
    }


    /**
     * @Description: 通过ID删除户信息
     * @Author: Heyue
     * @Date: 2020/6/21
     */
    @DeleteMapping("/deleteLogById/{tableId}")
    @ResponseBody
    public Object remove(@PathVariable("tableId") Integer tableId) {
        return xfService.deleteLogById(tableId);
    }


    /**
     * @Description: 批量删除
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @DeleteMapping("/batchLog")
    @ResponseBody
    public Object batchDelLive(@RequestParam("ids") String ids) {
        return xfService.batchLog(CommonUtil.StringToIntegerlIST(ids, "\\,"));
    }


    /**
     * @Description: 刑罚记录列表
     * @Author: Heyue
     * @Date: 2020/7/1
     */
    @GetMapping("/getXfjlList")
    @ResponseBody
    public Object getXfjlList(Integer page, Integer limit, String username, Integer statu) {
        return xfService.getXfjlList(new Pagination(page, limit), username, statu);
    }
}
