package org.cqipc.edu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.service.UserlifeService;
import org.cqipc.edu.utils.CommonUtil;
import org.cqipc.edu.vo.book.UserlifeAddVO;
import org.cqipc.edu.vo.book.UserlifeModifyVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 生薄控制器
 * @ClassName: UserlifeController
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Controller
@Transactional
public class UserlifeController {

    @Autowired(required = false)
    private UserlifeService userlifeService;


    /**
     * @Description: 生薄用户添加
     * @Author: Heyue
     * @Date: 2020/6/20
     */
    @PostMapping("/LivebookAdd/{mid}")
    @ResponseBody
    public Object LivebookAdd(@RequestParam("lifeInfo") String lifeInfo, @PathVariable("mid") Integer mid) throws JsonProcessingException {
        return userlifeService.LivebookAdd(new ObjectMapper().readValue(lifeInfo, UserlifeAddVO.class), mid);
    }


    /**
     * @Description: 分页/条件查询生薄信息
     * @Author: Heyue
     * @Date: 2020/6/20
     */
    @GetMapping("/getLivebookList")
    @ResponseBody
    public Object getLivebookList(Integer page, Integer limit, String username, Integer type) {
        return userlifeService.getLivebookList(new Pagination(page, limit), username, type);
    }


    /**
     * @Description: 通过表格中的ID删除生薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/21
     */
    @DeleteMapping("/deleteLivebookByid/{tableId}")
    @ResponseBody
    public Object remove(@PathVariable("tableId") Integer tableId) {
        return userlifeService.removeUserlifeById(tableId);
    }


    /**
     * @Description: 通过生薄用户ID查询对应
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @GetMapping("/getUserlifeById/{id}")
    @ResponseBody
    public Object getUserlifeById(@PathVariable("id") Integer id) {
        return userlifeService.getUserlifeById(id).getLifeType();
    }


    /**
     * @Description: 修改生薄用户信息
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @PutMapping("/livebookModify")
    @ResponseBody
    public Object modifyUserlife(@RequestParam("updateInfo") String updateInfo) throws JsonProcessingException {
        return userlifeService.modifyUserlife(new ObjectMapper().readValue(updateInfo, UserlifeModifyVO.class));
    }


    /**
     * @Description: 生薄用户批量删除
     * @Author: Heyue
     * @Date: 2020/6/22
     */
    @DeleteMapping("/batchDelLive")
    @ResponseBody
    public Object batchDelLive(@RequestParam("ids") String ids) {
        return userlifeService.batchDelLive(CommonUtil.StringToIntegerlIST(ids, "\\,"));
    }

}
