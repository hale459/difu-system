package org.cqipc.edu.controller;

import org.cqipc.edu.service.WelcomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description: 欢迎页控制器
 * @ClassName: WelcomeController
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Controller
public class WelcomeController {

    @Autowired
    private WelcomeService welcomeService;


    /**
     * @Description: 回显首页数据
     * @Author: Heyue
     * @Date: 2020/6/28
     */
    @GetMapping("/Initialization")
    @ResponseBody
    public Object Initialization() {
        return welcomeService.WelcomeData();
    }

}
