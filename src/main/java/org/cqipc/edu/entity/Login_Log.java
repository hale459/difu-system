package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 登录日志表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Login_Log implements Serializable {

    private Integer id;
    private String username;//用户名
    private String loginTime;//登录时间
    private String location;//登陆地点
    private String ip;//IP地址

}
