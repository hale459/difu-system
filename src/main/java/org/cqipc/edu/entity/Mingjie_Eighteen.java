package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 十八层地狱表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Mingjie_Eighteen implements Serializable {

    private Integer id;//主键ID
    private String eightName;//名称
    private String info;//描述
    private Integer level;//层级
    private String createTime;//创建时间

}
