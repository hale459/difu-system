package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 死亡名单表
 * @ClassName: Swmd
 * @Author: Heyue
 * @DateTime: 2020-06-26
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Swmd implements Serializable {

    private Integer id;//主键ID
    private Integer did;//被感染者ID
    private String dname;//姓名
    private String dsex;//性别
    private Integer dage;//年龄(当前年龄==死亡年龄)
    private String dtime;//死亡时间(创建时间)
    private Integer darea;//地区
    private Integer dtype;//类别
    private Integer dfzid;//分组ID

}
