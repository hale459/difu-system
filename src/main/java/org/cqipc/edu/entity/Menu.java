package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 菜单管理表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Menu implements Serializable {

    private BigInteger menuId;//菜单&ID按钮(主键)
    private BigInteger parentId;//上级菜单ID
    private String menuName;//菜单&按钮名称
    private String path;//对应路由path
    private String component;//对应路由组件component
    private String prems;//权限标识
    private String icon;//图标
    private String type;//类型(0:菜单  1:按钮)
    private double orderNum;//排序
    private String createTime;//创建时间
    private String modifyTime;//修改时间

}
