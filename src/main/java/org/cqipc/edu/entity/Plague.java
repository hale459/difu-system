package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 瘟疫状态表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Plague implements Serializable {

    private BigInteger plagueId;//瘟疫ID(主键)
    private String plagueName;//瘟疫名称
    private int plagueStatus;//瘟疫状态(1:可用  2:不可用)
    private String createTime;//创建时间
}
