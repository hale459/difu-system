package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 部门表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Dept implements Serializable {

    private BigInteger deptId;//部门ID(主键)
    private BigInteger parentId;//上级部门ID
    private String deptName;//部门名称
    private double orderNum;//排序
    private String createTime;//创建时间
    private String modifyTime;//修改时间

}
