package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 已删除用户表
 * @Author: Heyue
 * @Date: 2020/6/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Deleted implements Serializable {

    private Integer delId;//ID(主键)
    private Integer delDieId;//已删除用户ID
    private String delName;//已删除用户名
    private String delSex;//性别
    private Integer delCountAge;//总寿命
    private Integer delAge;//死时寿命
    private Integer delType;//死亡类型
    private Integer delWhy;//死亡原因
    private Integer delArea;//地区
    private String delReincarnation;//前世
    private Integer delReinState;//轮回状态(1:未轮回  2:已轮回)
    private Integer delStatu;//状态(1:生  2:死)
    private String delTime;//死亡时间
    private String delDesc;//死亡描述
    private String delCreatdate;//创建时间
    private String delLassTime;//最后编辑时间
    private String delTimeDel;//删除时间

}
