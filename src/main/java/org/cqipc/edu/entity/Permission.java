package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 权限表
 * @ClassName: Permission
 * @Author: Heyue
 * @DateTime: 2020-07-02
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Permission implements Serializable {

    private Integer id;
    private Integer roleId;
    private String perm;

}
