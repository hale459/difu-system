package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 系统设置表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class User_Config implements Serializable {

    private BigInteger userId;//用户ID(主键)
    private String theme;//系统主题 dark暗色风格，light明亮风格
    private String layout;//系统布局 side侧边栏，head顶部栏
    private String multiPage;//页面风格 1多标签页 0单页
    private String fixSiderbar;//页面滚动是否固定侧边栏 1固定 0不固定
    private String fixHeader;//页面滚动是否固定顶栏 1固定 0不固定
    private String color;//主题颜色 RGB值

}
