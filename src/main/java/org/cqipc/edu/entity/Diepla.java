package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 瘟疫表和死亡名单关联表  多对多
 * @ClassName: Diepla
 * @Author: Heyue
 * @DateTime: 2020-06-26
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Diepla implements Serializable {

    private Integer id;//主键
    private Integer pid;//瘟疫情况关联ID
    private Integer did;//死亡名单关联ID
    private String creatdate;//创建时间

}
