package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 类别表
 * @Author: Heyue
 * @Date: 2020/6/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Type implements Serializable {

    private Integer typeId;//类别ID
    private String typeName;//类别名称
    private Date typeCreatdate;//类别创建时间

}
