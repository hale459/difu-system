package org.cqipc.edu.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 分页封装(工具)
 * @ClassName: Pagination
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Pagination implements Serializable {

    private Integer page;
    private Integer pageCount;


    public Pagination(Integer page, Integer pageCount) {
        this.page = (page - 1) * pageCount;
        this.pageCount = pageCount;
    }
}
