package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 货币消费日志表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Mingjie_Hadescurrency_Log implements Serializable {

    private BigInteger id;//主键ID
    private BigInteger userId;//用户ID
    private String info;//描述
    private String createTime;//日志时间
    private int status;//状态(1:充值  2:消费)

}
