package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 死亡原因表
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Diewhy implements Serializable {

    private Integer id;//死亡原因ID
    private String name;//死亡原因名称
    private String creatdate;//创建时间

}
