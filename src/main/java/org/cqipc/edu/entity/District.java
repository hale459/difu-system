package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 地区联动表
 * @Author: Heyue
 * @Date: 2020/6/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class District implements Serializable {

    private Integer id;
    private Integer pid;
    private String districtName;
    private String type;
    private Integer hierarchy;
    private String districtSpe;

}
