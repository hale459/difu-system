package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 日志表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Log implements Serializable {

    private Integer id;//主键ID
    private String username;//用户名
    private String operation;//菜单项
    private String time;//时间
    private String method;//方法路径
    private String params;//参数
    private String ip;//IP地址
    private String createTime;//创建时间
    private String location;//位置

}
