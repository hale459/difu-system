package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 字典表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Dict implements Serializable {

    private BigInteger dictId;//字典ID(主键)
    private int keyy;//键
    private String valuee;//值
    private String fieldName;//字段名称
    private String tableName;//表名

}
