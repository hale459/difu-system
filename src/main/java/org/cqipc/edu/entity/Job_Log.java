package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 任务日志表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Job_Log implements Serializable {

    private BigInteger logId;//任务日志ID(主键)
    private BigInteger jobId;//任务ID
    private String beanName;//SpringBean名称
    private String methodName;//方法名
    private String params;//参数
    private String status;//状态(0:正常  1:暂停)
    private String error;//失败信息
    private double time;//耗时(单位:毫秒)
    private String createTime;//创建时间
}
