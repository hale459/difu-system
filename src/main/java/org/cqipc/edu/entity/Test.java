package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 测试表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Test implements Serializable {

    private BigInteger id;//主键ID
    private String field1;//文件1
    private int field2;//文件2
    private String field3;//文件3
    private String createTime;//创建时间

}
