package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 生命周期表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Mingjie_Lifeanddie implements Serializable {
    
    private BigInteger id;//主键ID
    private BigInteger userId;//用户ID
    private String userName;//用户姓名
    private String sex;//性别
    private String createTime;//创建时间
    private int age;//当前年龄
    private int totalAge;//总寿命
    private int overAge;//剩余寿命
    private int statu;//状态(1:生  2:死)

}
