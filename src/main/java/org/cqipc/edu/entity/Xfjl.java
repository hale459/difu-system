package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 刑罚记录
 * @ClassName: Xfjl
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Xfjl implements Serializable {

    private Integer xId;//主键ID
    private Integer xUserId;//受刑用户ID
    private String xUserName;//受刑用户
    private String xSxsj;//受刑时间
    private String xCjsj;//创建时间
    private Integer xDycj;//地狱层级
    private String xOntime;//入狱时间
    private String xOuttime;//出狱时间
    private Integer xStatu;//是否提前释放1是2否

}
