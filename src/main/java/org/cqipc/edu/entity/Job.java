package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 任务表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Job implements Serializable {

    private BigInteger jobId;//任务ID(主键)
    private String beanName;//SpringBean名称
    private String methodName;//方法名
    private String params;//参数
    private String cronExpression;//cron表达式
    private String status;//任务状态(0:正常  1:暂停)
    private String remark;//备注
    private String createTime;//创建时间

}
