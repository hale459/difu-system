package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 用户表
 * @Author: Heyue
 * @Date: 2020/6/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class User implements Serializable {

    private Integer userId;//用户ID(主键)
    private String username;//用户名
    private String birthday;//用户生日
    private String password;//密码
    private Integer deptId;//部门ID
    private String email;//邮箱
    private String mobile;//电话
    private Integer status;//状态(0:锁定  1:有效)
    private String createTime;//创建时间
    private String modifyTime;//修改时间
    private String lastLoginTime;//最近访问时间
    private String ssex;//性别(男:0  女:1  保密:2)
    private String description;//描述
    private String avatar;//头像
    private Integer age;//年龄

}
