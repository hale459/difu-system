package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 地区表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Area implements Serializable {

    private BigInteger id;//主键ID
    private int code;//地区code
    private String name;//地区名称
    private String info;//地区描述
    private String createTime;//创建时间
    private int fatherId;//父ID

}
