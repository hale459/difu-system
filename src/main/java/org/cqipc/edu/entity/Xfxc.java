package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 刑法现场表
 * @ClassName: Xfxc
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Xfxc implements Serializable {

    private Integer xId;//主键ID
    private Integer xUserId;//受刑用户ID
    private String xUserName;//受刑用户
    private String xSxsj;//受刑时间
    private String xCjsj;//创建时间
    private Integer xDycj;//地狱层级
    private String xOntime;//入狱时间

}
