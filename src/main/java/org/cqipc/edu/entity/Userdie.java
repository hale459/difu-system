package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 死薄表
 * @Author: Heyue
 * @Date: 2020/6/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Userdie implements Serializable {

    private Integer dieId;//死薄ID(主键)
    private String dieName;//死薄用户名
    private String dieSex;//死薄性别(0:男  1:女  2:保密)
    private Integer dieCountAge;//总寿命
    private Integer dieAge;//死时寿命
    private Integer dieType;//死亡类型
    private Integer dieWhy;//死亡原因
    private Integer dieArea;//地区
    private String dieReincarnation;//前世
    private Integer dieReinState;//轮回状态(1:未轮回  2:已轮回)
    private Integer dieStatu;//状态(1:生  2:死)
    private String dieTime;//死亡时间
    private String dieDesc;//死亡描述
    private String dieCreatdate;//创建时间
    private String dieLassTime;//最后编辑时间

}
