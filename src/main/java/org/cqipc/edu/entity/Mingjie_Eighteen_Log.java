package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 出入狱记录表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Mingjie_Eighteen_Log implements Serializable {

    private Integer id;//主键ID
    private Integer userId;//用户ID
    private String userName;//用户姓名
    private Integer eighteenId;//地狱ID
    private Integer status;//状态(1:入狱  2:出狱)
    private Date inTime;//入狱时间
    private Date outTime;//出狱时间
    private String inInfo;//入狱原因
    private String outInfo;//出狱原因
    private String createtime;//创建时间

}
