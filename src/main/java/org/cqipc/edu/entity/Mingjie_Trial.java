package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 被执事记录表被执事记录表(审判记录表)
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Mingjie_Trial implements Serializable {

    private Integer id;//主键ID
    private Integer userId;//被执行用户ID
    private String userName;//被执行用户姓名
    private Integer trialUserId;//执行者
    private String trialTime;//审判时间
    private String info;//审判描述
    private Integer type;//审判类型(1:待审  2:已审)
    private Integer types;//结果(0退回 1投胎 2地狱)

}
