package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @Description: 执行执事记录表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Mingjie_Soul implements Serializable {

    private BigInteger id;//主键ID
    private BigInteger userId;//被执行用户
    private BigInteger executorId;//执行者
    private String executorInfo;//执行描述
    private String executorTime;//执行时间
    private int executorStatus;//执行状态

}
