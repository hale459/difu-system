package org.cqipc.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 瘟疫情况表
 * @Author: Heyue
 * @Date: 2020/6/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Plague_Info implements Serializable {

    private Integer plagueInfoId;//主键id
    private Integer plagueId;//瘟疫ID
    private Integer areaId;//发生地区ID
    private String happenTime;//发生时间
    private String info;//描述
    private String name;//感染体姓名
    private Integer number;//感染人数
    private Integer dienumber;//死亡人数
    private Integer uid;//感染体ID

}
