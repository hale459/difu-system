package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Xfjl;

import java.util.List;

/**
 * @Description: 刑罚记录-数据接口
 * @ClassName: XfjlDao
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
public interface XfjlDao {

    //批量删除
    Integer batchDelXfjl(List<Integer> id);

    //删除记录
    Integer removeCfjlById(Integer id);

    //添加刑罚记录
    Integer addXfjl(Xfjl xfjl);

    //获取所以有刑罚记录
    List<Xfjl> getXfjlList(@Param("page") Pagination pagination, @Param("username") String username, @Param("statu") Integer statu);

    //获取所以有刑罚记录(总数)
    List<Xfjl> getXfjlListCount(@Param("username") String username, @Param("statu") Integer statu);
}
