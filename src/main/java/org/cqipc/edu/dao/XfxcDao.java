package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Xfxc;

import java.util.List;

/**
 * @Description: 刑法现场-数据接口
 * @ClassName: XfxcDao
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
public interface XfxcDao {

    //添加刑罚记录
    Integer addXfxc(Xfxc xfxc);

    //查询所有刑法现场记录
    List<Xfxc> getXfxcList(@Param("page") Pagination pagination, @Param("username") String username, @Param("level") Integer level);

    //查询所有刑法现场记录（总数）
    List<Xfxc> getXfxcListCount(@Param("username") String username, @Param("level") Integer level);

    //根据ID删除
    Integer delById(Integer id);

    //通过ID查询
    Xfxc getXfxcById(Integer id);


}
