package org.cqipc.edu.dao;

import org.cqipc.edu.entity.Plague;

import java.util.List;

/**
 * @Description: 瘟疫种类-数据接口
 * @ClassName: PlagueDao
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
public interface PlagueDao {

    //查询所有瘟疫信息
    List<Plague> getPlagueAll();

    //根据ID查询瘟疫信息
    Plague getPlagueById(Integer id);

    //根据瘟疫名称查询瘟疫信息
    List<Plague> getPlagueByName(String name);


}
