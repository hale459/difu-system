package org.cqipc.edu.dao;

import org.cqipc.edu.entity.Diewhy;

import java.util.List;

/**
 * @Description: 死亡原因-数据接口
 * @ClassName: DiewhyDao
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface DiewhyDao {

    //获取所有死亡原因信息
    List<Diewhy> getDiewhyAll();

    //根据ID获取死亡原因
    Diewhy getDiewhyById(Integer id);

    //根据Name获取死亡原因
    Diewhy getDiewhyByName(String name);


}
