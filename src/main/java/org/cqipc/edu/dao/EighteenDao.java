package org.cqipc.edu.dao;

import org.cqipc.edu.entity.Mingjie_Eighteen;

import java.util.List;

/**
 * @Description: 地狱层级-数据接口
 * @ClassName: EighteenDao
 * @Author: Heyue
 * @DateTime: 2020-06-29
 **/
public interface EighteenDao {

    //获取全部地狱层级信息
    List<Mingjie_Eighteen> getEighteenLevel();

}
