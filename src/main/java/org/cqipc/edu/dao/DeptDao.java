package org.cqipc.edu.dao;

import org.cqipc.edu.entity.Dept;

import java.util.List;

/**
 * @Description: 部门-数据接口
 * @ClassName: DeptDao
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
public interface DeptDao {

    //获取部门信息
    List<Dept> getDeptList();

}
