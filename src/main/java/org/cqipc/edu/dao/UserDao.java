package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.User;

import java.util.List;

/**
 * @Description: 用户-数据接口
 * @ClassName: UserDao
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface UserDao {

    //查询用户条数
    List<User> getUserAllSize();

    //分页/条件查询用户信息
    List<User> getUserAll(@Param("page") Pagination pagination, @Param("id") Integer id, @Param("username") String username);

    //通过条件查询后的总数
    List<User> getCountPage(@Param("id") Integer id, @Param("username") String username);

    //根据用户ID获取用户信息
    User getUserById(Integer id);

    //根据用户Name获取用户信息
    List<User> getUserByName(String name);

    //用户登录
    User UserLogin(@Param("username") String username, @Param("password") String password);

    //通过姓名查询信息
    User getUserByUserName(String username);

    //批量删除--用户信息
    Integer batchDelUser(List<Integer> item);

    //根据用户ID删除用户信息
    Integer removeUserById(Integer id);

}
