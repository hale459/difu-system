package org.cqipc.edu.dao;

import org.cqipc.edu.entity.Type;

import java.util.List;

/**
 * @Description: 类别-数据接口
 * @ClassName: TypeDao
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface TypeDao {

    //获取所有类别
    List<Type> getTypeAll();

    //根据类型ID拿到对应类型
    Type getTypeById(Integer id);

    //根据类型Name拿到对应类型
    Type getTypeByName(String name);

}
