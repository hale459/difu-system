package org.cqipc.edu.dao;

import org.cqipc.edu.entity.Role;

import java.util.List;

/**
 * @Description: 角色-数据接口
 * @ClassName: RoleDao
 * @Author: Heyue
 * @DateTime: 2020-07-02
 **/
public interface RoleDao {

    //通过用户ID查询该用户的角色信息
    List<Role> getRoleByRoleId(Integer id);

}
