package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Userdie;

import java.util.List;

/**
 * @Description: 死薄用户-数据接口
 * @ClassName: UserdieDao
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface UserdieDao {

    //分页/条件查询死薄信息
    List<Userdie> getDiebookList(@Param("page") Pagination pagination, @Param("username") String username, @Param("type") Integer type,
                                 @Param("dieWhy") Integer dieWhy);

    //条件查询死薄信息
    List<Userdie> getCountPage(@Param("username") String username, @Param("type") Integer type,
                               @Param("dieWhy") Integer dieWhy);

    //获取所有死薄信息
    List<Userdie> getUserdieAll();

    //分页查询所有未分配的用户
    List<Userdie> getUnassigned(@Param("page") Integer page, @Param("pageCount") Integer limit,
                                @Param("unassigned") Integer unassigned);

    //查询所有死亡后未分配的用户
    List<Userdie> getUnassignedAll(Integer unassigned);

    //根据死薄用户ID信息查询信息
    Userdie getUserdieById(Integer id);

    //修改死薄用户信息
    Integer modifyUserdie(Userdie userdie);

    //通过死薄ID删除用户信息
    Integer removeUserdieById(Integer id);

    //死薄批量删除
    Integer batchDelDie(List<Integer> item);

    //死薄批量添加
    Integer batchAddDie(List<Userdie> userdie);

    //添加死薄用户(主要是在已删除表中退回)
    Integer insertUserDieBack(Userdie userdie);

    //添加死薄用户
    Integer addUserdie(Userdie userdie);

    //通过ID修改轮回状态
    Integer updateUserdieReinId(Integer id);
}
