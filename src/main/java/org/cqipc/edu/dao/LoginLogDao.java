package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Login_Log;
import org.cqipc.edu.entity.Pagination;

import java.util.List;

/**
 * @Description: 登录日志-数据接口
 * @ClassName: LoginLogDao
 * @Author: Heyue
 * @DateTime: 2020-07-01
 **/
public interface LoginLogDao {

    //查询登录日志
    List<Login_Log> getLoginLogList(@Param("page") Pagination pagination, @Param("username") String username,
                                    @Param("date") String date);

    //查询登录日志  总数
    List<Login_Log> getLoginLogListCout(@Param("username") String username,
                                        @Param("date") String date);

    //通过ID删除
    Integer removeLoginLogById(Integer id);

    //批量删除
    Integer batchDelLoginLog(List<Integer> id);

    //添加记录
    Integer addLoginLog(Login_Log login_log);



}
