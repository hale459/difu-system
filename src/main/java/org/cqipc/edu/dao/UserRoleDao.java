package org.cqipc.edu.dao;

import org.cqipc.edu.entity.User_Role;

/**
 * @Description: 用户角色关系-数据接口
 * @ClassName: UserRoleDao
 * @Author: Heyue
 * @DateTime: 2020-07-02
 **/
public interface UserRoleDao {

    //通过用户ID查询该用户的角色ID
    User_Role getUserRoleByUserId(Integer id);

}
