package org.cqipc.edu.dao;

import org.cqipc.edu.entity.Permission;

import java.util.List;

/**
 * @Description: 权限-数据接口
 * @ClassName: PermissionDao
 * @Author: Heyue
 * @DateTime: 2020-07-02
 **/
public interface PermissionDao {

    //通过角色ID查询这个角色的所有权限
    List<Permission> getPermissionByRoleId(Integer id);

}
