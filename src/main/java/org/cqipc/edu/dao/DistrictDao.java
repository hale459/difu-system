package org.cqipc.edu.dao;

import org.cqipc.edu.entity.District;

import java.util.List;

/**
 * @Description: 地区联动-数据接口
 * @ClassName: DistrictDao
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface DistrictDao {

    //获取编号为1和2的地区
    List<District> getAllList();

    //通过地区ID查询地区信息
    District getDistrictById(Integer id);

}
