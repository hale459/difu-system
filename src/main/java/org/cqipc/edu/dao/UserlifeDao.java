package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Userlife;

import java.util.List;

/**
 * @Description: 生薄用户-数据接口
 * @ClassName: UserlifeDao
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public interface UserlifeDao {

    //查询所有(不分页)
    List<Userlife> getLivebookAll();

    //查询已死亡人的总数
    List<Userlife> getUnassignedAll();

    //添加生薄用户
    Integer LivebookAdd(Userlife userlife);

    //分页/条件查询生薄信息
    List<Userlife> getLivebookList(@Param("page") Pagination pagination, @Param("username") String username,
                                   @Param("type") Integer type);

    //条件查询生薄信息
    List<Userlife> getCountPage(@Param("username") String username, @Param("type") Integer type);

    //查询所有生薄信息
    List<Userlife> getUserlifeAll();

    //通过生薄ID删除用户信息
    Integer removeUserlifeById(Integer id);

    //根据ID信息查询信息
    Userlife getUserlifeById(Integer id);

    //修改生薄用户信息
    Integer modifyUserlife(Userlife userlife);

    //生薄批量删除
    Integer batchDelLive(List<Integer> item);

    //根据地区ID查询用户信息
    List<Userlife> getUserlifeByAreaId(Integer id);

    //查询已经死亡的人
    List<Userlife> getUnassigned(@Param("page") Pagination pagination, @Param("username") String username, @Param("date") String date,
                                 @Param("statu") Integer statu);

    //查询已经死亡的人(总数)
    List<Userlife> getUnassignedPage(@Param("username") String username, @Param("date") String date,
                                     @Param("statu") Integer statu);


}
