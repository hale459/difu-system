package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Plague_Info;

import java.util.List;

/**
 * @Description: 瘟疫情况-数据接口
 * @ClassName: PlagueInfoDao
 * @Author: Heyue
 * @DateTime: 2020-06-25
 **/
public interface PlagueInfoDao {

    //添加瘟疫情况信息
    Integer addPlagueInfo(Plague_Info plagueInfo);

    //分页/条件查询瘟疫情况
    List<Plague_Info> getPlagueInfoList(@Param(("page")) Pagination pagination, @Param("username") String username, @Param("plague") Integer plague, @Param("area") Integer area);

    //条件查询瘟疫情况总数
    List<Plague_Info> getPlagueInfoCount(@Param("username") String username, @Param("plague") Integer plague, @Param("area") Integer area);

    //批量删除瘟疫情况表
    Integer batchDelPlagueInfo(List<Integer> id);

    //通过ID删除瘟疫信息
    Integer removePlaugeInfoByid(Integer id);

    //通过ID查询瘟疫信息
    Plague_Info getPlaugeInfoById(Integer id);

}