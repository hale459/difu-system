package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Deleted;
import org.cqipc.edu.entity.Pagination;

import java.util.List;

/**
 * @Description: 已删除用户-数据接口
 * @ClassName: DeletedDao
 * @Author: Heyue
 * @DateTime: 2020-06-23
 **/
public interface DeletedDao {

    //已删除用户添加(主要来源于在死薄中删除的用户信息)
    Integer deletedAdd(Deleted deleted);

    //分页/条件查询已删除用户信息
    List<Deleted> getDeletedList(@Param("page") Pagination pagination, @Param("id") Integer id, @Param("username") String username);

    //查询所有已删除用户
    List<Deleted> getDeletedAll();

    //批量删除已删除用户
    Integer batchDelDeleted(List<Integer> item);

    //通过ID删除已删除用户信息(永久删除, 无法轮回)
    Integer removeDeletedById(Integer id);

    //根据ID拿到已删除用户的信息
    Deleted getDeletedById(Integer id);

}
