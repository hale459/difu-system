package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Mengpo;
import org.cqipc.edu.entity.Pagination;

import java.util.List;

/**
 * @Description: 孟婆操作-数据接口
 * @ClassName: MengpoDao
 * @Author: Heyue
 * @DateTime: 2020-06-29
 **/
public interface MengpoDao {

    //分页/条件查询所有孟婆信息
    List<Mengpo> getMengpoPage(@Param("page") Pagination pagination, @Param("diewhy") Integer diewhy, @Param("username") String username);

    //条件查询所有孟婆信息
    List<Mengpo> getMengpoPageCount(@Param("diewhy") Integer diewhy, @Param("username") String username);

    //添加孟婆信息
    Integer addMengpo(Mengpo mengpo);

    //通过ID查询孟婆信息
    Mengpo getMengpoById(Integer id);

    //通过ID修改轮回状态
    Integer updateMengpoReinId(Integer mid);

}
