package org.cqipc.edu.dao;

import org.cqipc.edu.entity.Mingjie_Trial;

/**
 * @Description:审判记录-数据接口
 * @ClassName: TrialDao
 * @Author: Heyue
 * @DateTime: 2020-06-28
 **/
public interface TrialDao {

    //批量添加审判记录
    Integer addTrial(Mingjie_Trial trials);

    //通过ID查询审判记录
    Mingjie_Trial getTrialByUserId(Integer id);

}
