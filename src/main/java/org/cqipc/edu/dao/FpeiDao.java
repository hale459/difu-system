package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Fpei;
import org.cqipc.edu.entity.Pagination;

import java.util.List;

/**
 * @Description: 分配-数据接口
 * @ClassName: FpeiDao
 * @Author: Heyue
 * @DateTime: 2020-06-27
 **/
public interface FpeiDao {

    //批量添加到分配表
    Integer batchAddFpei(List<Fpei> fpeis);

    //通过ID获取分配信息
    Fpei getFpeiById(Integer id);

    //分页/条件查询已分配的人
    List<Fpei> getFpeiPage(@Param("page") Pagination pagination, @Param("statu") Integer statu, @Param("username") String username);

    //条件查询已分配的人的总数
    List<Fpei> getFpeiCount(@Param("statu") Integer statu, @Param("username") String username);

    //批量修改分配状态
    Integer batchModByStatu(List<Fpei> fp);

    //通过ID删除
    Integer delFpeiById(Integer id);
}
