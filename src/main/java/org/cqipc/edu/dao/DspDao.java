package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Dsp;
import org.cqipc.edu.entity.Pagination;

import java.util.List;

/**
 * @Description: 待审判-数据接口
 * @ClassName: DspDao
 * @Author: Heyue
 * @DateTime: 2020-06-27
 **/
public interface DspDao {

    //批量添加
    Integer batchAddDsp(List<Dsp> dsp);

    //查询所有待审判的人(默认状态为1)
    List<Dsp> getDspAll();

    //分页/条件获取【待审判】表格数据
    List<Dsp> getDspList(@Param("page") Pagination pagination, @Param("username") String username, @Param("date") String date,
                         @Param("statu") Integer statu);

    //条件获取【待审判】表格数据总数
    List<Dsp> getDspListCount(@Param("username") String username, @Param("date") String date, @Param("statu") Integer statu);

    //通过ID删除【待审判记录】
    Integer delDspById(Integer id);

    //批量删除【已审判记录】
    Integer batchDelDsp(List<Integer> ids);

    //修改审判状态
    Integer updateDspStatu(@Param("statu") Integer statu, @Param("id") Integer id);

    //通过ID查询待审判用户
    Dsp getDspById(Integer id);
}
