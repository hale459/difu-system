package org.cqipc.edu.dao;

import org.apache.ibatis.annotations.Param;
import org.cqipc.edu.entity.Pagination;
import org.cqipc.edu.entity.Swmd;

import java.util.List;

/**
 * @Description: 死亡名单-数据接口
 * @ClassName: SwmdDao
 * @Author: Heyue
 * @DateTime: 2020-06-26
 **/
public interface SwmdDao {

    //批量添加死亡名单
    Integer batchAddSwmd(List<Swmd> swmd);

    //批量删除死亡名单信息
    Integer batchDelSwmd(List<Integer> id);

    //通过分组ID查询信息
    List<Swmd> getPlaugeInfoByFzid(Integer id);

    //分页查询死亡名单
    List<Swmd> getSwmdPage(@Param("page") Pagination pagination, @Param("id") Integer id);

    //分页查询死亡名单总数
    List<Swmd> getSwmdPageCount(Integer id);

}
