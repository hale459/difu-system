package org.cqipc.edu.utils;

import org.springframework.util.DigestUtils;

/**
 * @Description: MD5加密工具类
 * @ClassName: Md5
 * @Author: Heyue
 * @DateTime: 2020-06-20
 **/
public class Md5 {
    public static String getMd5(String Text) {
        return DigestUtils.md5DigestAsHex(Text.getBytes());
    }

}
