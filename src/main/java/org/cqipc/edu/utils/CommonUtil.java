package org.cqipc.edu.utils;

import java.util.*;

/**
 * @Description: 公共工具类
 * @ClassName: CommonUtil
 * @Author: Heyue
 * @DateTime: 2020-06-24
 **/
public class CommonUtil {

    /**
     * @Description: 将前端传过来的id字符串封装成Integer集合并返回(批量删除)
     * @Author: Heyue
     * @Date: 2020/6/24
     */
    public static List<Integer> StringToIntegerlIST(String ids, String term) {
        String idString[] = ids.trim().split(term);
        List<Integer> list = new ArrayList<>();
        for (String id : idString) {
            list.add(Integer.parseInt(id));
        }
        return list;
    }


    /**
     * @Description: 生成range范围内的num个随机数
     * @Author: Heyue
     * @Date: 2020/6/26
     */
    public static Set<Integer> randomNum(Integer num, Integer range) {
        Integer n = 0;
        Random random = new Random();
        Set<Integer> set = new HashSet<Integer>();
        while (n < num) {
            int temp = random.nextInt(range);
            set.add(temp);
            n = set.size();
        }
        return set;
    }

}
